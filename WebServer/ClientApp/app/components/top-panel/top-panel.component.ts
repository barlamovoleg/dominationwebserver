﻿import { Component, EventEmitter, Output } from '@angular/core';
import { VideoGridsService } from "../../services/video-grids-service";
import { GridType, VideoGrid } from "../../models/gridTypes";

@Component({
    selector: 'app-top-panel',
    templateUrl: './top-panel.component.html',
    styleUrls: ['./top-panel.component.scss']
})
export class TopPanelComponent {

    @Output() onSettingsButtonClick = new EventEmitter();

    private get allVideoGrids(): VideoGrid[] {
        return this.videoGridsService.allGrids;
    }

    private get selectedVideoGrid(): VideoGrid {
        return this.videoGridsService.selectedVideoGrid;
    }

    private set selectedVideoGrid(videoGrid: VideoGrid) {
       this.videoGridsService.selectedVideoGrid = videoGrid;
    }

    constructor(private videoGridsService: VideoGridsService) {
    }

    isVideoGridSelected(videoGrid: VideoGrid): boolean {
        return this.selectedVideoGrid === videoGrid;
    }

    selectVideoGrid(videoGrid: VideoGrid) {
        this.selectedVideoGrid = videoGrid;
    }

    settingsButtonClick() {
        this.onSettingsButtonClick.emit();
    }
}