﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Common.Annotations;

namespace Common
{
    public class CollectionChangedHandler<T> : IDisposable
    {
        private INotifyCollectionChanged Collection { get; }
        public Action<IEnumerable<T>> OnAddedAction { get; }
        public Action<IEnumerable<T>> OnRemovedAction { get; }

        public CollectionChangedHandler([NotNull] INotifyCollectionChanged collection, [NotNull] Action<IEnumerable<T>> onAddedAction, Action<IEnumerable<T>> onRemovedAction = null)
        {
            Collection = collection ?? throw new ArgumentNullException(nameof(collection));
            OnAddedAction = onAddedAction ?? throw new ArgumentNullException(nameof(onAddedAction));
            OnRemovedAction = onRemovedAction;
            Collection.CollectionChanged += CollectionOnCollectionChanged;
        }

        private void CollectionOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    OnAddedAction(e.NewItems.Cast<T>());
                    break;
                case NotifyCollectionChangedAction.Move:
                    break;
                case NotifyCollectionChangedAction.Remove:
                    OnRemovedAction?.Invoke(e.NewItems.Cast<T>());
                    break;
                case NotifyCollectionChangedAction.Replace:
                    break;
                case NotifyCollectionChangedAction.Reset:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Dispose()
        {
            Collection.CollectionChanged -= CollectionOnCollectionChanged;
        }
    }
}
