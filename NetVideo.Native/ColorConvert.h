#pragma once

#include "stdafx.h"

struct ConversionParams
{
	uint8_t* srcData[8];
	int32_t srcStride[8];
	int32_t srcWidth;
	int32_t srcHeight;
	AVPixelFormat srcFormat;

	uint8_t* dstData[8];
	int32_t dstStride[8];
	int32_t dstWidth;
	int32_t dstHeight;
	AVPixelFormat dstFormat;

	bool invertYUV420Planes;
};

class ColorConvert
{
private:
	static void InvertYuvPlane(uint8_t plane[], int32_t planeSize);
	static AVPixelFormat FixYUVJ(AVPixelFormat decodedFormat);

public:
	static int32_t Convert(ConversionParams* params);
};