import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { ChannelsProvider } from "./services/channels-provider-service";
import { VideoGridsService } from "./services/video-grids-service";

import { AppComponent } from './components/app/app.component';
import { HomeComponent } from './components/home/home.component';
import { LeftPanelComponent } from "./components/left-panel/left-panel.component";
import { TopPanelComponent } from "./components/top-panel/top-panel.component";
import { SettingsWindowComponent } from "./components/settings-window/settings-window.component";
import { ChannelsTreeComponent } from "./components/channels-tree/channels-tree.component";
import { VideoGridComponent } from "./components/video-grid/video-grid.component";
import { VideoViewerComponent } from "./components/video-viewer/video-viewer.component";
import { ClockComponent } from "./components/clock/clock.component";
import { Grid1x1Component } from "./components/grids/grid1x1/grid1x1.component";
import { Grid2x2Component } from "./components/grids/grid2x2/grid2x2.component";
import { Grid5Component } from "./components/grids/grid5/grid5.component";
import { Grid6Component } from "./components/grids/grid6/grid6.component";
import { Grid3x3Component } from "./components/grids/grid3x3/grid3x3.component";
import { Grid4x4Component } from "./components/grids/grid4x4/grid4x4.component";
import { TreeviewComponent } from "./components/treeview/treeview.component";
import { GridCellComponent } from "./components/grid-cell/grid-cell.component";
import { CellStubComponent } from "./components/cell-stub/cell-stub.component";

import { Injectable } from '@angular/core';
import { RequestOptions, BaseRequestOptions, Headers } from '@angular/http';

@Injectable()
export class CustomRequestOptions extends BaseRequestOptions {
    headers = new Headers({
        'Cache-Control': 'no-cache',
        'Pragma': 'no-cache',
        'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT'
    });
}


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        LeftPanelComponent,
        TopPanelComponent,
        SettingsWindowComponent,
        ChannelsTreeComponent,
        VideoGridComponent,
        VideoViewerComponent,
        ClockComponent,
        Grid1x1Component,
        Grid2x2Component,
        Grid5Component,
        Grid6Component,
        Grid3x3Component,
        Grid4x4Component,
        TreeviewComponent,
        GridCellComponent,
        CellStubComponent,
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        BrowserModule,
        HttpModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            //{ path: 'fetch-data', component: FetchDataComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ],
    providers: [VideoGridsService, ChannelsProvider, { provide: RequestOptions, useClass: CustomRequestOptions }]
})
export class AppModuleShared {
}