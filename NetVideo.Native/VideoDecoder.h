#pragma once

#include "stdafx.h"
#include "Decoder.h"

class VideoDecoder
{
private:
    AVCodecContext * _context;

public:
    explicit VideoDecoder();
    virtual ~VideoDecoder();

    DecoderError OpenDecoderContext(AVCodec* codec, AVCodecParameters* parameters);

    int32_t Push(uint8_t data[], int32_t size) const;
    int32_t Push(const AVPacket* packet) const;
    int32_t Pop(AVFrame* frame) const;
};
