﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services
{
    public interface IEnvironmentService
    {
        string TempFolderPath { get; }

        string ConfigFolderPath { get; }

        string LibsPath { get; }
    }
}
