﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Channels;
using System.Threading.Tasks;
using Common.Annotations;

namespace Core
{
    public class StreamWrapper : Stream
    {
        public override bool CanRead => _memoryStream?.CanRead ?? false;
        public override bool CanSeek => _memoryStream?.CanSeek ?? false;
        public override bool CanWrite => _memoryStream?.CanWrite ?? false;
        public override long Length => _memoryStream?.Length ?? 0;

        public override long Position
        {
            get { return _memoryStream?.Position ?? 0; }
            set
            {
                if (_memoryStream != null)
                    _memoryStream.Position = value;
            }
        }

        public ChannelReader<byte[]> ChannelReader { get; }

        private MemoryStream _memoryStream;

        public StreamWrapper([NotNull] ChannelReader<byte[]> channelReader)
        {
            ChannelReader = channelReader ?? throw new ArgumentNullException(nameof(channelReader));

            ReadStreamAsync().GetAwaiter();
        }

        private async Task ReadStreamAsync()
        {
            await Task.Delay(1).ConfigureAwait(false);

            var buffer = new List<byte[]>();

            while (true)
            {
                var readBytes = await ChannelReader.ReadAsync();
                if (readBytes == null)
                    break;

                buffer.Add(readBytes);
            }

            //WTF!!!
            var fullArray = Combine(buffer.ToArray());

            _memoryStream = new MemoryStream(fullArray);
        }

        private byte[] Combine(params byte[][] arrays)
        {
            byte[] rv = new byte[arrays.Sum(a => a.Length)];
            int offset = 0;
            foreach (byte[] array in arrays)
            {
                System.Buffer.BlockCopy(array, 0, rv, offset, array.Length);
                offset += array.Length;
            }
            return rv;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (_memoryStream == null)
                return 0;

            return _memoryStream.Read(buffer, offset, count);
        }

        public override void Flush()
        {
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return 0;
        }

        public override void SetLength(long value)
        {
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
        }
    }
}
