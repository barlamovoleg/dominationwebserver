﻿using System;

namespace NetVideo.Codec
{
    /// <summary> Contains YUV color space tools. </summary>
    public static class Yuv
    {
        /// <summary> Convert unmanaged YUV frame to a managed RGB24 frame. </summary>
        /// <param name="frame"> Unmanaged YUV pixels. </param>
        /// <returns> Managed array of RGB24 pixels. </returns>
        public static unsafe byte[] ToRgb24(DecodedFrame frame)
        {
            int uvHeight = frame.Height/2;
            int uvWidth = frame.Width/2;
            int width = frame.Width;

            const int pixelSize = 3;
            int rgbStride = pixelSize*width;
            byte[] buffer = new byte[rgbStride*frame.Height];

            byte* ptr = (byte*) frame.Data0;
            {
                for (int y = 0; y < uvHeight; y++)
                {
                    IntPtr data = new IntPtr(ptr);

                    byte* y1Plane = (byte*) data + (2*y)*width;
                    byte* y2Plane = (byte*) data + (2*y + 1)*width;
                    byte* uPlane = null;//(byte*) frame.Plane1 + h*uvWidth;
                    byte* vPlane = null;//(byte*) frame.Plane2 + h*uvWidth;

                    int i = (2*y)*rgbStride;
                    int j = i + rgbStride;
                    for (int x = 0; x < uvWidth; x++)
                    {
                        int y11 = *y1Plane++; int y12 = *y1Plane++;
                        int y21 = *y2Plane++; int y22 = *y2Plane++;

                        int v = *vPlane++ - 128;
                        int u = *uPlane++ - 128;

                        //    1f,     1f,         1f,
                        //    0f,     -0.344f,    1.772f,
                        //    1.403f, -0.714f,    0f
                        double rChroma = 1.403*v;
                        double gChroma = -0.714*v - 0.344*u;
                        double bChroma = 1.772*u;

                        buffer[i++] = Clip(y11 + rChroma); buffer[j++] = Clip(y21 + rChroma);
                        buffer[i++] = Clip(y11 + gChroma); buffer[j++] = Clip(y21 + gChroma);
                        buffer[i++] = Clip(y11 + bChroma); buffer[j++] = Clip(y21 + bChroma);

                        buffer[i++] = Clip(y12 + rChroma); buffer[j++] = Clip(y22 + rChroma);
                        buffer[i++] = Clip(y12 + gChroma); buffer[j++] = Clip(y22 + gChroma);
                        buffer[i++] = Clip(y12 + bChroma); buffer[j++] = Clip(y22 + bChroma);
                    }
                }
            }

            return buffer;
        }

        /// <summary> Запихать любое значение в 0-255. </summary>
        private static byte Clip(double value)
        {
            return value < byte.MinValue ? byte.MinValue
                : (value > byte.MaxValue ? byte.MaxValue
                                         : (byte)value);
        }
    }
}