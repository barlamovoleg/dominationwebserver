﻿using Common.Annotations;
using Core;
using Microsoft.Extensions.DependencyInjection;

namespace NetVideoServer
{
    [UsedImplicitly]
    public class DominationModule : IServicesModule
    {
        public void RegisterServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IServerConnector, DominationServerConnector>();
        }
    }
}
