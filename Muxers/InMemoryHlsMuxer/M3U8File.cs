﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Muxers.InMemoryHlsMuxer
{
    internal class M3U8File : IDisposable
    {
        public int MaxChunksCount { get; }

        public int CurrentChunksCount => _chunksLines.Count;

        private readonly object _chunksLocker = new object();
        private readonly List<string> _chunksLines = new List<string>();

        public M3U8File(int maxChunksCount)
        {
            MaxChunksCount = maxChunksCount;
        }

        public void Dispose()
        {
        }

        public string GetContent()
        {
            var content = GetHeader(_hlsVersion, _allowCache, _targetChunkDurationSeconds, _counter);

            string[] lines;
            lock (_chunksLocker)
            {
                lines = _chunksLines.ToArray();
            }

            foreach (var line in lines)
            {
                content += line;
            }

            return content;
        }

        private int _hlsVersion;
        private bool _allowCache;
        private int _targetChunkDurationSeconds;

        private int _counter;

        public Task InitializeAsync(int hlsVersion, bool allowCache, int targetChunkDurationSeconds, CancellationToken cancellationToken)
        {
            _hlsVersion = hlsVersion;
            _allowCache = allowCache;
            _targetChunkDurationSeconds = targetChunkDurationSeconds;
            return Task.CompletedTask;
        }

        public Task AddChunkAsync(string chunkName, double durationInSeconds, CancellationToken cancellationToken)
        {
            lock (_chunksLocker)
            {
                var chunkLine = GetChunkData(chunkName, durationInSeconds);
                _chunksLines.Add(chunkLine);
                _counter++;

                var first = _chunksLines.First();
                if (_chunksLines.Count > MaxChunksCount)
                {
                    _chunksLines.Remove(first);
                }
            }

            return Task.CompletedTask;
        }

        private string GetHeader(int hlsVersion, bool allowCache, int targetChunkDurationSeconds, int counter)
        {
            var ln = Environment.NewLine;
            return "#EXTM3U" + ln +
                   $"#EXT-X-VERSION:{hlsVersion}" + ln +
                   $"#EXT-X-MEDIA-SEQUENCE:{counter}" + ln +
                   $"#EXT-X-ALLOW-CACHE:{Bool2Str(allowCache)}" + ln +
                   $"#EXT-X-TARGETDURATION:{targetChunkDurationSeconds}" + ln;
        }

        private string GetChunkData(string chunkName, double duration)
        {
            var ln = Environment.NewLine;
            return $"#EXTINF:{duration:0.######}," + ln +
                   chunkName + ln;
        }

        private static string Bool2Str(bool value)
        {
            return value ? "YES" : "NO";
        }
    }
}
