﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Common.Annotations;
using Core.Logging;
using Core.Services;

namespace ServerLogic
{
    internal class ServerCore : IDisposable
    {
        private IConfigProvider ConfigProvider { get; }
        private AllServersConnector AllServersConnector { get; }
        public IChannelsProvider ChannelsProvider { get; }
        public IVideoPlayersService VideoPlayersService { get; }

        public ServerCore([NotNull] IConfigProvider configProvider, [NotNull] AllServersConnector allServersConnector, [NotNull] IChannelsProvider channelsProvider, [NotNull] IVideoPlayersService videoPlayersService)
        {
            ConfigProvider = configProvider ?? throw new ArgumentNullException(nameof(configProvider));
            AllServersConnector = allServersConnector ?? throw new ArgumentNullException(nameof(allServersConnector));
            ChannelsProvider = channelsProvider ?? throw new ArgumentNullException(nameof(channelsProvider));
            VideoPlayersService = videoPlayersService ?? throw new ArgumentNullException(nameof(videoPlayersService));
        }

        public async Task RunAsync(CancellationToken cancellationToken)
        {
            var config = await ConfigProvider.GetConfigAsync(cancellationToken).ConfigureAwait(false);
            var serversInfos = config.ServersInfos;

            cancellationToken.ThrowIfCancellationRequested();

            Logger.Log("connecting to servers...");
            await AllServersConnector.ConnectServersAsync(serversInfos, cancellationToken).ConfigureAwait(false);
        }

        public void Dispose()
        {
            
        }
    }
}
