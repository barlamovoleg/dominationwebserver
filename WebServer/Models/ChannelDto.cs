﻿using Common.Annotations;

namespace WebServer.Models
{
    public class ChannelDto
    {
        [PublicAPI]
        public string Name { get; set; }

        [PublicAPI]
        public string Location { get; set; }

        [PublicAPI]
        public string Marker { get; set; }
    }
}
