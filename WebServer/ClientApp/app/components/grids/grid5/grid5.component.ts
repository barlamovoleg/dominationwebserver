﻿import { Component } from '@angular/core';
import { GridBase } from "../gridBase";

@Component({
    selector: 'app-grid5',
    templateUrl: './grid5.component.html',
    styleUrls: ['./grid5.component.scss']
})

export class Grid5Component extends GridBase {

    constructor() {
        super();
    }
}