﻿import { Channel } from "./channels";

export enum GridType {
    Grid1x1 = 0,
    Grid2x2,
    Grid5,
    Grid6,
    Grid3x3,
    Grid4x4,
}

export class VideoGrid {

    public gridCells: GridCell[];

    public cellsCount : number;

    constructor(public gridType: GridType = GridType.Grid1x1, public iconPath: string) {
        var cellsCount = this.getGridTypeCellsCount(gridType);
        this.cellsCount = cellsCount;

        this.gridCells = new Array<GridCell>(cellsCount);
        for (var i = 0; i < cellsCount; i++) {
            this.gridCells[i] = new GridCell(i);
        }
    }

    public findCellByIndex(index: number): GridCell | null {
        for (var cell of this.gridCells) {
            if (cell.index === index)
                return cell;
        }

        return null;
    }

    private getGridTypeCellsCount(gridType: GridType) : number {
        if (gridType === GridType.Grid1x1)
            return 1;
        if (gridType === GridType.Grid2x2)
            return 4;
        if (gridType === GridType.Grid5)
            return 5;
        if (gridType === GridType.Grid6)
            return 6;
        if (gridType === GridType.Grid3x3)
            return 9;
        if (gridType === GridType.Grid4x4)
            return 16;

        return -1;
    }

}

export enum GridCellMode {
    Undefined,
    Realtime,
    Archive
}

export class GridCell {

    public channel : Channel | null;
    public mode: GridCellMode;

    constructor(public index: number) {
        this.channel = null;
        this.mode = GridCellMode.Realtime;
    }

    public isEmpty(): boolean {
        return this.channel === null;
    }
}

