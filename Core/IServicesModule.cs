﻿using Microsoft.Extensions.DependencyInjection;

namespace Core
{
    public interface IServicesModule
    {
        void RegisterServices(IServiceCollection serviceCollection);
    }
}
