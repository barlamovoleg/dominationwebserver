﻿using System;
using System.Threading.Channels;
using Common.Annotations;
using Core;
using Core.Logging;
using Core.Messages;
using NetVideo;

namespace NetVideoServer
{
    internal class VideoSource : IRawVideoSource
    {
        public RealtimeVideoSubscription Subscription { get; }
        public ChannelReader<VideoMessage> VideoStream => _videoSource.Reader;
        private ChannelWriter<VideoMessage> VideoStreamWriter => _videoSource.Writer;

        private readonly Channel<VideoMessage> _videoSource = Channel.CreateUnbounded<VideoMessage>(new UnboundedChannelOptions
        {
            SingleReader = false,
            SingleWriter = true,
            AllowSynchronousContinuations = false
        });

        public VideoSource([NotNull] RealtimeVideoSubscription subscription)
        {
            Subscription = subscription ?? throw new ArgumentNullException(nameof(subscription));
            Subscription.NewMedia += SubscriptionOnNewMedia;
        }

        private async void SubscriptionOnNewMedia(object sender, MediaPacket e)
        {
            try
            {
                if (e is NetVideoImage image)
                {
                    var videoMessage = image.ToVideoMessage();
                    await VideoStreamWriter.WriteAsync(videoMessage).ConfigureAwait(false);
                }
            }
            catch (OperationCanceledException)
            {

            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
        }

        public void Dispose()
        {
            Subscription.NewMedia -= SubscriptionOnNewMedia;
            Subscription.Close();
        }
    }
}
