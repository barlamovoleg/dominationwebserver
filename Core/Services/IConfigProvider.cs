﻿using System.Threading;
using System.Threading.Tasks;
using Core.Config;

namespace Core.Services
{
    public interface IConfigProvider
    {
        Task<WebServerConfig> GetConfigAsync(CancellationToken cancellationToken);
    }
}
