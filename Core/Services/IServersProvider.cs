﻿using System;
using System.Collections.Generic;

namespace Core.Services
{
    public interface IServersProvider : IDisposable
    {
        event EventHandler<IServer> ServerConnected;

        event EventHandler<IServer> ServerDisconnected;

        IReadOnlyList<IServer> GetConnectedServers();
    }
}
