﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { GridCell } from "../../models/gridTypes";
import { Channel } from "../../models/channels";

@Component({
    selector: 'app-grid-cell',
    templateUrl: './grid-cell.component.html',
    styleUrls: ['./grid-cell.component.scss']
})
export class GridCellComponent {

    @Input()
    public gridCell: GridCell | undefined = undefined;

    @Output()
    onCellsSwap = new EventEmitter();

    private allowDrag: boolean;

    constructor() {
        this.allowDrag = false;
    }

    private onDropChannel(droppedChannel: Channel) {
        var cell = this.gridCell as GridCell;
        if (cell == null)
            return;

        cell.channel = droppedChannel;
        this.allowDrag = droppedChannel != null && droppedChannel != undefined;
    }

    private onDropCell(droppedCell: GridCell) {
        var cell = this.gridCell as GridCell;
        if (cell == null)
            return;

        if (cell.index === droppedCell.index)
            return;

        var eventArgs = new SwapCellsEventArgs(cell, droppedCell);
        this.onCellsSwap.emit(eventArgs);
    }

    private onDrop(event: DragEvent) {
        var canDrop = this.canDropData(event);
        if (!canDrop)
            return;

        if (this.onDropNode(event))
            return;

        if (this.onDropGridCell(event))
            return;
    }

    private onDropNode(event: DragEvent): boolean {
        var jsonData = event.dataTransfer.getData("text");
        if (jsonData == undefined || jsonData === "") {
            return false;
        }

        var nodeContext = JSON.parse(jsonData);
        var droppedChannel = nodeContext as Channel;
        if (droppedChannel != null && droppedChannel.location != undefined) {
            this.onDropChannel(droppedChannel);
            return true;
        }

        return false;
    }

    private onDropGridCell(event: DragEvent): boolean {
        var jsonData = event.dataTransfer.getData("text");
        if (jsonData == undefined || jsonData === "") {
            return false;
        }

        var nodeContext = JSON.parse(jsonData);
        var droppedCell = nodeContext as GridCell;
        if (droppedCell != null) {
            this.onDropCell(droppedCell);
            return true;
        }

        return false;
    }

    private onDragOver(event: DragEvent) {
        var canDrop = this.canDropData(event);
        if (canDrop) {
            event.preventDefault();
        }
    }

    private canDropData(event: DragEvent) {
        //don't work on ie11 and edge

        //var isTreeNodeExists = event.dataTransfer.types.indexOf("tree_node") >= 0;
        //if (isTreeNodeExists)
        //    return true;
        //var isGridCellExists = event.dataTransfer.types.indexOf("grid_cell") >= 0;
        //if (isGridCellExists) {
        //    var notNullGridCell = this.gridCell as GridCell;
        //    if (notNullGridCell === null || notNullGridCell == undefined)
        //        return false;

        //    var indexData = this.getIndexData(notNullGridCell);

        //    //the same index
        //    if (event.dataTransfer.types.indexOf(indexData) >= 0)
        //        return false;

        //    return true;
        //}

        //return false;
        return true;
    }

    private canDrag(event: DragEvent): boolean {
        var canDrag = this.gridCell != null && !this.gridCell.isEmpty();

        if (!canDrag)
            return false;
        else {
            var notNullGridCell = this.gridCell as GridCell;
            if (notNullGridCell == null)
                return false;

            event.dataTransfer.effectAllowed = 'move';
            var dataContext = JSON.stringify(notNullGridCell);
            event.dataTransfer.setData("text", dataContext);
            //event.dataTransfer.setData("grid_cell", dataContext);
            //var gridIndexData = this.getIndexData(notNullGridCell);
            //event.dataTransfer.setData(gridIndexData, gridIndexData);

            return true;
        }
    }

    private getIndexData(gridCell: GridCell): string {
        return "grid_index_" + gridCell.index;
    }
}

export class SwapCellsEventArgs {
    constructor(public targetGridCell: GridCell, public sourceGridCell: GridCell) {
    }
}