﻿import { Component } from '@angular/core';
import { GridBase } from "../gridBase";

@Component({
    selector: 'app-grid4x4',
    templateUrl: './grid4x4.component.html',
    styleUrls: ['./grid4x4.component.scss']
})
export class Grid4x4Component extends GridBase {

    constructor() {
        super();
    }
}