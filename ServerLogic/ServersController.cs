﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.Config;
using Core.Logging;
using Core.Services;

namespace ServerLogic
{
    public class ServersController : IServersProvider
    {
        public event EventHandler<IServer> ServerConnected;
        public event EventHandler<IServer> ServerDisconnected;

        private readonly List<IServer> _connectedServers = new List<IServer>();
        private readonly object _connectedServersLocker = new object();

        private bool _isDisposed;

        public Task ConnectServersAsync(IReadOnlyList<ServerInfo> servers, IReadOnlyList<IServerConnector> connectors, CancellationToken cancellationToken)
        {
            var tasks = new List<Task>();
            foreach (var serverInfo in servers)
            {
                var targetConnector = connectors.SingleOrDefault(connector => connector.TargetServerType == serverInfo.TargetServerType);
                if (targetConnector != null)
                {
                    var connectTask = targetConnector.ConnectToServerAsync(serverInfo, cancellationToken).ContinueWith(task =>
                    {
                        OnServerConnected(task.Result);
                    }, cancellationToken, TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler.Current);
                    tasks.Add(connectTask);
                }
            }

            return Task.WhenAll(tasks);
        }

        public IReadOnlyList<IServer> GetConnectedServers()
        {
            lock (_connectedServersLocker)
            {
                return new List<IServer>(_connectedServers);
            }
        }

        public void Dispose()
        {
            _isDisposed = true;

            lock (_connectedServers)
            {
                foreach (var server in _connectedServers)
                {
                    DestroyServer(server);
                }

                _connectedServers.Clear();
            }
        }

        private void OnServerConnected(IServer server)
        {
            lock (_connectedServersLocker)
            {
                if (_isDisposed) return;

                _connectedServers.Add(server);


                ServerConnected?.Invoke(this, server);

                server.Disconnected += ServerOnDisconnected;
                server.ConnectionRestored += ServerOnConnectionRestored;
                server.ConnectionLost += ServerOnConnectionLost;

                Logger.Log($"server {server} has been connected");
            }
        }

        private void ServerOnConnectionLost(object sender, EventArgs e)
        {
            lock (_connectedServersLocker)
            {
                var server = (IServer) sender;

                Logger.Log($"server {server} connection lost");

                _connectedServers.Remove(server);
                ServerDisconnected?.Invoke(this, server);
            }
        }

        private void ServerOnConnectionRestored(object sender, EventArgs e)
        {
            lock (_connectedServersLocker)
            {
                if (_isDisposed) return;

                var server = (IServer) sender;

                Logger.Log($"server {server} connection restored");

                _connectedServers.Add(server);
                ServerConnected?.Invoke(this, server);
            }
        }

        private void ServerOnDisconnected(object sender, EventArgs e)
        {
            var server = (IServer) sender;
            DestroyServer(server);
        }


        private void DestroyServer(IServer server)
        {
            lock (_connectedServersLocker)
            {
                server.Disconnected -= ServerOnDisconnected;
                server.ConnectionRestored -= ServerOnConnectionRestored;
                server.ConnectionLost -= ServerOnConnectionLost;

                _connectedServers.Remove(server);
                ServerDisconnected?.Invoke(this, server);

                server.Dispose();
            }

            Logger.Log($"server {server} has been destroyed");
        }
    }
}
