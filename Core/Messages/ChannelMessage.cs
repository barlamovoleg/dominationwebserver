﻿using System;

namespace Core.Messages
{
    public abstract class ChannelMessage
    {
        /// <summary> Channel identifier. </summary>
        public int ChannelID { get; set; }

        /// <summary> Audio packet timestamp. </summary>
        public DateTime Timestamp { get; set; }

        /// <summary> Linked key frame index. </summary>
        public int KeyFrameIdx { get; set; }

        /// <summary> Frame raw data. </summary>
        public IFrameData Data { get; set; }

        public int SequenceID { get; set; }
    }
}
