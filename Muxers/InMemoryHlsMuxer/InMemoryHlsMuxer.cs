﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Core.Logging;
using Core.Messages;
using Microsoft.Extensions.Logging;

namespace Muxers.InMemoryHlsMuxer
{
    internal class InMemoryHlsMuxer : IIndexedMuxer, IMuxerWithChunks
    {
        public Guid Id { get; } = Guid.NewGuid();

        public Stream OutputStream => GetMuxedData();

        private ChannelReader<VideoMessage> VideoFramesReader { get; set; }

        private int MaxChunksCount { get; }
        private TimeSpan MaxChunkDuration { get; }

        private readonly M3U8File _m3U8File;
        private readonly ChunksCollection _chunksCollection;
        private readonly ChunkDurationWatcher _chunkDurationWatcher;

        private MpegTsChunk _currentChunk;
        private int _currentIndex;

        private CancellationTokenSource _muxingCancellationTokenSource;

        public InMemoryHlsMuxer(int maxChunksCount, TimeSpan maxChunkDuration)
        {
            MaxChunksCount = maxChunksCount;
            MaxChunkDuration = maxChunkDuration;
            _m3U8File = new M3U8File(MaxChunksCount);
            _chunksCollection = new ChunksCollection(MaxChunksCount);
            _chunkDurationWatcher = new ChunkDurationWatcher(MaxChunkDuration);
        }

        public Stream GetMuxedData()
        {
            var fileContent = _m3U8File.GetContent();
            var bytes = Encoding.UTF8.GetBytes(fileContent);
            return new MemoryStream(bytes);
        }

        public void StartMuxing(ChannelReader<VideoMessage> inputRawVideo)
        {
            StopMuxing();

            VideoFramesReader = inputRawVideo ?? throw new ArgumentNullException(nameof(inputRawVideo));

            _muxingCancellationTokenSource = new CancellationTokenSource();

            StartMuxingAsync(_muxingCancellationTokenSource.Token).GetAwaiter();
        }

        public void StopMuxing()
        {
            try
            {
                if (_muxingCancellationTokenSource != null)
                {
                    _muxingCancellationTokenSource.Cancel();
                    _muxingCancellationTokenSource.Dispose();
                }
            }
            catch (Exception)
            {
                //ignore
            }
        }

        public async Task WaitFirstChunkAsync(CancellationToken cancellationToken)
        {
            while (_m3U8File.CurrentChunksCount < 1)
                await Task.Delay(100, cancellationToken).ConfigureAwait(false);
        }

        public Stream GetChunkData(int index)
        {
            var chunk = _chunksCollection.GetLastChunk();
            if (chunk == null)
            {
                Logger.Log($"unknown chunk index {index}", LogLevel.Error);
                return new MemoryStream();
            }

            return new MemoryStream(chunk.Data);
        }

        public async Task StartMuxingAsync(CancellationToken cancellationToken)
        {
            await _m3U8File.InitializeAsync(3, false, (int)Math.Round(MaxChunkDuration.TotalSeconds), cancellationToken).ConfigureAwait(false);

            DateTime? previousTimeStamp = null;
            long timeMs = 0;

            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();

                var message = await VideoFramesReader.ReadAsync(cancellationToken).ConfigureAwait(false);

                if (_chunkDurationWatcher.IsNeedNewChunk && message.IsKeyFrame)
                {
                    _currentChunk.CloseChunk();
                    previousTimeStamp = _currentChunk.EndTimeStamp;
                    timeMs = _currentChunk.TimeStampMp;

                    var chunkName = GetChunkNameFromIndex(_currentIndex);
                    await _m3U8File.AddChunkAsync(chunkName, MaxChunkDuration.TotalSeconds, cancellationToken).ConfigureAwait(false);
                    _chunksCollection.AddChunk(_currentChunk, _currentIndex);

                    _chunkDurationWatcher.OnNewChunk();
                    _currentChunk = null;
                }

                if (_currentChunk == null)
                {
                    _currentIndex++;
                    _currentChunk = new MpegTsChunk();
                    await _currentChunk.InitializeAsync(cancellationToken, previousTimeStamp, timeMs).ConfigureAwait(false);
                }

                if (_currentChunk.IsEmpty && !message.IsKeyFrame)
                    continue;

                var newData = message.Data;
                await _currentChunk.PushDataAsync(newData, message.Timestamp, message.IsKeyFrame, cancellationToken).ConfigureAwait(false);

                _chunkDurationWatcher.AddFrame(message);
            }
        }

        public string GetChunkNameFromIndex(int chunkIndex)
        {
            return $"../../chunk/muxer_{Id:N}/chunk_{chunkIndex}";
        }

        public void Dispose()
        {
            StopMuxing();

            _m3U8File.Dispose();
            _chunksCollection.Dispose();
        }
    }
}
