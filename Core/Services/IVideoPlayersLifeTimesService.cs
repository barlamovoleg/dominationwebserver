﻿using System;

namespace Core.Services
{
    public interface IVideoPlayersLifeTimesService : IDisposable
    {
        event EventHandler<IVideoPlayer> PlayerLifeTimeExpired;

        void CreateLifeTime(IVideoPlayer videoPlayer);

        void ResetLifeTime(IVideoPlayer videoPlayer);
    }
}
