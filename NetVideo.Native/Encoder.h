#pragma once

#include "stdafx.h"

enum class EncoderError
{
	NoErrors = 0,
	FindEncoder = 1,
	AllocContext = 2,
	CodecOpen = 3,
	ImageAlloc = 4,
	GuessFormat = 5,
	NewStream = 6
};

// ����������� �������� ��� �������������������/���������������������.
typedef void(*MuxStreamCB)(uint8_t buffer [], int32_t size);

// ����� ���������� �����������/�������������.
class Encoder
{
private:
	// ������� �����������.
	AVCodecID _codecID;
	AVCodec* _encoder; // ���������� � �����������.
	AVCodecContext* _context; // �������� �����������.
	AVFrame* _frame; // �������������� ����.

	// ������� ��������������.
	AVOutputFormat* _format; // ���������� � ������� ����������.
	AVFormatContext* _formatContext; // �������� ����������.
	AVStream* _stream; // ���������� � ������.
	uint8_t* _streamBuffer; // ����� ������.

	// ����������� ��� �������.
	void CloseAll();
	// ����������� ������� ������.
	void CloseStream();
	// ����������� ��� ������� �����������.
	void CloseEncoder();

public:
	// ������������� muxing stream callback ��� ������ ������ ����������.
	// callback ���������� ������������� ��� �������������������/���������������������.
	MuxStreamCB muxStreamCallback;
	
	// �����������.
	Encoder(AVCodecID codecId);

	// ����������.
	virtual ~Encoder();

	// �������������� ����������.
	// gopSize ��������� ���������� ������ � ������ ������.
	// ������ ������ ������������ ����� ���� �������� ���� 
	// � ������������ ����������� (������-�������).
	EncoderError OpenEncoder(int32_t width, int32_t height, int32_t fps, int32_t bitRate, int32_t gopSize);

	// ������� ����� ����������.
	EncoderError CreateMuxStream(const char* containerName, int32_t bufferSize);
	
	// �������� ����.
	// ���������� ����� ������ packet.
	int32_t Encode(uint8_t data[], int64_t timestamp, int32_t& keyFrame) const;

	// ���������� ������ ������ � ���������.
	// (���������� muxStreamCallback).
	int32_t WriteHeader() const;

	// ���������� ��������� ������ � ���������.
	// (���������� muxStreamCallback).
	int32_t WriteTrailer() const;
};
