﻿import { Component } from '@angular/core';
import { GridBase } from "../gridBase";

@Component({
    selector: 'app-grid3x3',
    templateUrl: './grid3x3.component.html',
    styleUrls: ['./grid3x3.component.scss']
})
export class Grid3x3Component extends GridBase {

    constructor() {
        super();
    }
}