﻿export interface ITreeNode {
    isExpanded: boolean;
    name: string;
    children: Array<ITreeNode>;
    pathToIcon: string;
    context: object | null;
    canDrag() : boolean;
}

export class TreeNode implements ITreeNode {

    public isExpanded: boolean;
    public pathToIcon: string;
    public context: object | null;
    public draggable: boolean = false;

    constructor(public name: string,public children: ITreeNode[] = []) {
        this.isExpanded = true;
        this.pathToIcon = "";
        this.context = null;
    }

    canDrag(): boolean {
        return this.draggable;
    }

}