﻿namespace NetVideo.Codec
{
    /// <summary> Enumerates supported codec types. </summary>
    public enum CodecType
    {
        /// <summary> Unknown codec. Reserved. </summary>
        Unknown = 0,
        
        /// <summary> H.264 codec. </summary>
        H264 = 10,

        /// <summary> H.265 codec. </summary>
        H265 = 11,

        /// <summary> MJPEG codec. </summary>
        MJPEG = 20,

        /// <summary> MPEG4 codec. </summary>
        MPEG4 = 34,

        /// <summary> WebM codec. </summary>
        WEBM = 100,

        GSM = 1000,
        MuLaw = 1001,
        ALaw = 1002,

        G726_16k = 1003,
        G726_24k = 1004,
        G726_32k = 1005,
        G726_40k = 1006,
        
        G729 = 1007,

        PCM = 1100,
    }
}
