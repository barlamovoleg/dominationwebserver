﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Logging;

namespace Muxers
{
    public class MuxersCollection : IDisposable
    {
        private readonly object _muxersLocker = new object();
        private readonly List<IIndexedMuxer> _muxers = new List<IIndexedMuxer>();

        public IIndexedMuxer FindMuxer(Guid id)
        {
            lock (_muxersLocker)
            {
                return _muxers.FirstOrDefault(muxer => muxer.Id == id);
            }
        }

        /// <summary>
        /// "muxer_{Id:N}/chunk_{chunkIndex}";
        /// </summary>
        public Stream GetChunkData(string chunkMarker)
        {
            if (string.IsNullOrWhiteSpace(chunkMarker)) throw new ArgumentException(nameof(chunkMarker));

            var splitted = chunkMarker.Split('/');
            if (splitted.Length < 2)
                throw new ArgumentException(nameof(chunkMarker));

            var muxerIdStr = splitted[0];
            var chunkStr = splitted[1];

            var muxerID = ParseMuxerId(muxerIdStr, "muxer_");
            var chunkIndex = ParseChunkIndex(chunkStr, "chunk_");

            var muxer = FindMuxer(muxerID);
            if (muxer == null)
            {
                Logger.Log($"unknown muxer {muxerIdStr}");
                return new MemoryStream();
            }

            if (muxer is IMuxerWithChunks muxerWithChunks)
            {
                return muxerWithChunks.GetChunkData(chunkIndex);
            }

            Logger.Log($"muxer {muxerIdStr} not have any chunks");
            return new MemoryStream();

        }

        //muxer_{Id:N}
        private static Guid ParseMuxerId(string muxerIdReqStr, string pattern)
        {
            if (string.IsNullOrWhiteSpace(muxerIdReqStr)) throw new ArgumentException(nameof(muxerIdReqStr));

            var patternStr = pattern;
            var startIndex = muxerIdReqStr.IndexOf(patternStr, StringComparison.Ordinal);
            if (startIndex < 0)
                throw new ArgumentException(muxerIdReqStr);

            var idStr = muxerIdReqStr.Substring(startIndex + patternStr.Length);
            return Guid.Parse(idStr);
        }

        //chunk_{chunkIndex}
        private static int ParseChunkIndex(string chunkIndexReqStr, string pattern)
        {
            if (string.IsNullOrWhiteSpace(chunkIndexReqStr)) throw new ArgumentException(nameof(chunkIndexReqStr));
            var patternStr = pattern;
            var startIndex = chunkIndexReqStr.IndexOf(patternStr, StringComparison.Ordinal);
            if (startIndex < 0)
                throw new ArgumentException(chunkIndexReqStr);

            var indexStr = chunkIndexReqStr.Substring(startIndex + patternStr.Length);
            return int.Parse(indexStr);
        }

        public void AddMuxer(IIndexedMuxer muxer)
        {
            lock (_muxersLocker)
            {
                _muxers.Add(muxer);
            }
        }

        public void RemoveMuxer(IIndexedMuxer muxer)
        {
            lock (_muxersLocker)
            {
                _muxers.Remove(muxer);
            }
        }

        public void Dispose()
        {
            lock (_muxersLocker)
            {
                _muxers.Clear();
            }
        }
    }
}
