﻿using System;
using System.Runtime.InteropServices;

namespace Core.Messages
{
    public interface IFrameData
    {
        int Size { get; }

        bool IsEmpty { get; }
    }

    public static class FrameDataFactory
    {
        public static IFrameData FromBytes(byte[] data)
        {
            return ToNative(data);
        }

        public static IFrameData FromBytes(ArraySegment<byte> data)
        {
            return ToNative(data);
        }



        private static IFrameData ToNative(byte[] data)
        {
            var handle = new SafeNativeMemoryHandle(data.Length);

            Marshal.Copy(data, 0, handle.DangerousGetHandle(), data.Length);

            return handle;
        }

        private static IFrameData ToNative(ArraySegment<byte> data)
        {
            var handle = new SafeNativeMemoryHandle(data.Count);

            Marshal.Copy(data.Array, data.Offset, handle.DangerousGetHandle(), data.Count);

            return handle;
        }
    }
}
