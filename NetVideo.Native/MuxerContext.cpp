#include "stdafx.h"
#include "MuxerContext.h"

static int write_packet(void *opaque, uint8_t *buf, int buf_size)
{
	IOOutput* out = reinterpret_cast<IOOutput*>(opaque);
	memcpy(out->outBuffer + out->bytesSet, buf, buf_size);
	out->bytesSet += buf_size;
	return buf_size;
}

static int write_packet2(void *opaque, uint8_t *buf, int buf_size)
 {
	struct buffer_data *bd = (struct buffer_data *)opaque;
	while (buf_size > bd->room) {
		int64_t offset = bd->ptr - bd->buf;
		bd->buf = static_cast<uint8_t *>(av_realloc_f(bd->buf, 2, bd->size));
		if (!bd->buf)
			 return AVERROR(ENOMEM);
		bd->size *= 2;
		bd->ptr = bd->buf + offset;
		bd->room = bd->size - offset;
		
	}
	//printf("write packet pkt_size:%d used_buf_size:%zu buf_size:%zu buf_room:%zu\n", buf_size, bd->ptr - bd->buf, bd->size, bd->room);
	
	/* copy buffer data to buffer_data buffer */
	memcpy(bd->ptr, buf, buf_size);
	bd->ptr += buf_size;
	bd->room -= buf_size;
	
	return buf_size;
}

MuxerContext::MuxerContext() : _context(nullptr)
{
	avio_ctx_buffer_size = 4096;
	bd = { 0 };

	_avio_ctx_buffer = static_cast<uint8_t *>(av_malloc(avio_ctx_buffer_size));

	bd.ptr = bd.buf = static_cast<uint8_t *>(av_malloc(bd_buf_size));
	bd.size = bd.room = bd_buf_size;
}

MuxerContext::~MuxerContext()
{
	avformat_free_context(_context);

	if (_avioContext)
	{
		av_freep(&_avio_ctx_buffer);
		av_freep(&_avioContext);
	}
	
	av_freep(&bd.buf);
}

MuxerError MuxerContext::Open(AVOutputFormat* muxerType)
{
	if (muxerType == nullptr)
		return MuxerError::MuxerTypeNotSet;

	if (avformat_alloc_output_context2(&_context, muxerType, NULL, NULL) != 0)
		return MuxerError::MuxerOpenError;

	if (_context == nullptr)
		return MuxerError::MuxerOpenError;


	AVCodecParameters* codecParameters = avcodec_parameters_alloc();
	codecParameters->codec_id = AV_CODEC_ID_H264;
	_stream = avformat_new_stream(_context, NULL);
	if (_stream == nullptr)
		return MuxerError::MuxerOpenError;

	_stream->codecpar = codecParameters;

	_avioContext = avio_alloc_context(_avio_ctx_buffer, avio_ctx_buffer_size,
		+1, &bd, NULL, &write_packet2, NULL); //= avio_alloc_context(_avio_ctx_buffer, 4096, 1, outptr, NULL, write_packet, NULL);
	if (_avioContext == nullptr)
		return MuxerError::MuxerOpenError;

	_context->pb = _avioContext;

	if (avformat_write_header(_context, NULL) != 0)
		return MuxerError::WriteHeadersError;

	return MuxerError::Success;
}

MuxerError MuxerContext::Push(uint8_t data[], const int32_t size, const long timestamp, bool isKeyFrame)
{
	if (_context == nullptr)
		return MuxerError::MustOpenBeforeError;

	AVPacket packet;

	av_init_packet(&packet);

	if (isKeyFrame)
		packet.flags |= AV_PKT_FLAG_KEY;

	packet.data = data;
	packet.size = size;
	packet.pts = packet.dts = timestamp;

	//can be not interleaved for increase perfomance
	int result = av_interleaved_write_frame(_context, &packet);
	if (result < 0)
		return MuxerError::WriteDataError;

	return MuxerError::Success;
}

MuxerError MuxerContext::Close(uint8_t outputData[]) const
{
	av_write_trailer(_context);

	/* Free the streams. */
	for (int i = 0; i < _context->nb_streams; i++) {
		 av_freep(&_context->streams[i]);
	}

	memcpy(outputData, bd.buf, bd.size);

	return MuxerError::Success;
}

const int MuxerContext::GetLength()
{
	return bd.size;
}
