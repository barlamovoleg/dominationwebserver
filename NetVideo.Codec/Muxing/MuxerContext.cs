﻿using System;
using Core.Logging;
using Microsoft.Win32.SafeHandles;
using NetVideo.Codec.Interop;

namespace NetVideo.Codec.Muxing
{
    public class MuxerContext : SafeHandleZeroOrMinusOneIsInvalid
    {
        public int OutputDataLength => NativeMethods.GetLength(handle);

        public MuxerType MuxerType { get; }

        public long TimeStampMs { get; private set; }

        public DateTime? LastTimeStamp { get; private set; }

        public MuxerContext(MuxerType muxerType)
            : base(true)
        {
            try
            {
                MuxerType = muxerType;
                IntPtr muxerContext = NativeMethods.CreateMuxerContext();

                SetHandle(muxerContext);
            }
            catch (Exception e)
            {
                Logger.Log(e);
            }
        }

        public void Initialize(DateTime? previousTimeStamp, long timeStampMs = 0)
        {
            TimeStampMs = timeStampMs;
            LastTimeStamp = previousTimeStamp;

            var error = NativeMethods.OpenMuxer(handle, MuxerType);
            if (error != MuxerError.Success)
                throw new MuxingException(error);
        }

        public void Push(IntPtr data, int dataLength, DateTime timeStamp, bool isKeyFrame)
        {
            var resultTimeStamp = GetTimeStamp(timeStamp);

            var error = NativeMethods.Push(handle, data, dataLength, resultTimeStamp, isKeyFrame);
            if (error != 0)
                throw new MuxingException(error);
        }

        public byte[] CloseMuxer()
        {
            var outputArray = new byte[OutputDataLength];

            unsafe
            {
                fixed (byte* p = outputArray)
                {
                    IntPtr ptr = (IntPtr)p;
                    var error = NativeMethods.Close(handle, ptr);
                    if (error != 0)
                        throw new MuxingException(error);
                }
            }

            return outputArray;
        }

        protected override bool ReleaseHandle()
        {
            NativeMethods.DeleteMuxerContext(handle);
            return true;
        }

        private long GetTimeStamp(DateTime newFrameTimeStamp)
        {
            if (!LastTimeStamp.HasValue)
            {
                LastTimeStamp = newFrameTimeStamp;
                TimeStampMs = 0;
                return 0;
            }

            var diff = newFrameTimeStamp - LastTimeStamp.Value;
            LastTimeStamp = newFrameTimeStamp;

            var diffMs = (long)diff.TotalMilliseconds;
            TimeStampMs += diffMs;
            return MsToPts(TimeStampMs);
        }

        private long MsToPts(long ms)
        {
            unchecked
            {
                //Fucking magic! time_base in av_format_stream reset to 1/90000!!!
                return ms * 90000 / 1000 ;
            }
        }
    }
}
