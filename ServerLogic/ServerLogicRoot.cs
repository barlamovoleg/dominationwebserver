﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Common.Annotations;
using Core;
using Core.Logging;
using Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace ServerLogic
{
    public class ServerLogicRoot : IDisposable
    {
        [NotNull] private StartupParameters StartupParameters { get; }

        private ServerCore _serverCore;

        public ServerLogicRoot([NotNull] StartupParameters startupParameters)
        {
            StartupParameters = startupParameters ?? throw new ArgumentNullException(nameof(startupParameters));
        }

        public void Dispose()
        {
            _serverCore.Dispose();
        }

        public async Task PreInitAsync(CancellationToken cancellationToken)
        {
            await PreloadAssembliesAsync(cancellationToken).ConfigureAwait(false);

            cancellationToken.ThrowIfCancellationRequested();
        }

        public async Task RegisterServicesAsync(IServiceCollection serviceCollection, CancellationToken cancellationToken)
        {
            await RegisterModulesFromDomainAsync(serviceCollection, cancellationToken).ConfigureAwait(false);

            cancellationToken.ThrowIfCancellationRequested();

            serviceCollection.AddSingleton<ServerCore>();
        }

        public async Task PostInitAsync(IServiceProvider serviceProvider, CancellationToken cancellationToken)
        {
            //load config
            var configProviderService = serviceProvider.GetRequiredService<IConfigProvider>();
            await configProviderService.GetConfigAsync(cancellationToken).ConfigureAwait(false);

            cancellationToken.ThrowIfCancellationRequested();

            _serverCore = serviceProvider.GetService<ServerCore>();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return _serverCore.RunAsync(cancellationToken);
        }

        private async Task PreloadAssembliesAsync(CancellationToken cancellationToken)
        {
            var pluginsFolderPath = StartupParameters.PluginsFolderPath;
            if (!string.IsNullOrWhiteSpace(pluginsFolderPath) && Directory.Exists(pluginsFolderPath))
                await LoadAssembliesFromFolderAsync(pluginsFolderPath, cancellationToken).ConfigureAwait(false);

            var rootFolderPath = StartupParameters.RootAssembliesFolderPath;
            if (!string.IsNullOrWhiteSpace(rootFolderPath) && Directory.Exists(rootFolderPath))
                await LoadAssembliesFromFolderAsync(rootFolderPath, cancellationToken).ConfigureAwait(false);

            cancellationToken.ThrowIfCancellationRequested();
        }

        private Task LoadAssembliesFromFolderAsync(string pathToFolder, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                var assemblyFiles = Directory.GetFiles(pathToFolder, "*.dll", SearchOption.AllDirectories);

                foreach (var assemblyFile in assemblyFiles)
                {
                    try
                    {
                        Assembly.LoadFrom(assemblyFile);
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    cancellationToken.ThrowIfCancellationRequested();
                }
            }, cancellationToken);
        }

        private Task RegisterModulesFromDomainAsync(IServiceCollection serviceCollection, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                var domainModulesTypes = AssemblyHelper.GetAllImplementingInterface<IServicesModule>();

                foreach (var domainModuleType in domainModulesTypes)
                {
                    try
                    {
                        var instance = (IServicesModule) Activator.CreateInstance(domainModuleType);
                        instance.RegisterServices(serviceCollection);
                    }
                    catch (Exception e)
                    {
                        Logger.Log(e, $"Error while registering module {domainModuleType}");
                    }

                    cancellationToken.ThrowIfCancellationRequested();
                }
            }, cancellationToken);
        }

    }
}
