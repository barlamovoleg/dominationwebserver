﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.Logging;
using Core.Messages;
using NetVideo.Codec.Muxing;

namespace Muxers.InMemoryHlsMuxer
{
    internal class MpegTsChunk : IDisposable
    {
        public byte[] Data { get; private set; }

        public int SizeInBytes => _currentSize;

        public DateTime EndTimeStamp { get; private set; }

        public long TimeStampMp { get; private set; }

        public bool IsEmpty { get; private set; } = true;

        private int _currentSize;

        private readonly MuxerContext _muxerContext;

        private bool _isClosed;

        public MpegTsChunk()
        {
            _muxerContext = new MuxerContext(MuxerType.Mpegts);
        }

        public Task InitializeAsync(CancellationToken cancellationToken, DateTime? previousTimeStamp = null, long timeStampMs = 0)
        {
            try
            {
                _muxerContext.Initialize(previousTimeStamp, timeStampMs);
            }
            catch (Exception e)
            {
                Logger.Log(e, "Failed to initialize muxer");
            }

            return Task.CompletedTask;
        }

        public Task PushDataAsync(IFrameData data, DateTime timeStamp, bool isKeyFrame, CancellationToken cancellationToken)
        {
            try
            {
                if (data is SafeNativeMemoryHandle native)
                {
                    //byte[] managedArray = new byte[native.Size];
                    //Marshal.Copy(native.DangerousGetHandle(), managedArray, 0, native.Size);
                    //_rawdataBytes.AddRange(managedArray);

                    PushData(native.DangerousGetHandle(), native.Size, timeStamp, isKeyFrame);
                    Interlocked.Add(ref _currentSize, data.Size);

                    IsEmpty = false;
                }
                else
                    throw new Exception("UNKNOWN DATA");
            }
            catch (Exception e)
            {
                Logger.Log(e, "Failed to push data to muxer");
            }

            return Task.CompletedTask;
        }

        public void CloseChunk()
        {
            _isClosed = true;

            try
            {
                Data = _muxerContext.CloseMuxer();

                EndTimeStamp = _muxerContext.LastTimeStamp.Value;
                TimeStampMp = _muxerContext.TimeStampMs;

                _muxerContext.Dispose();

                //File.WriteAllBytes(@"D:\ffmpeg\bin\raw.h264", _rawdataBytes.ToArray());
                //File.WriteAllBytes($@"D:\ffmpeg\bin\out{++_index}.ts", Data);
                //await _mpegTsOutput.Writer.WriteAsync(output, cancellationToken).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logger.Log(e, "Error while closing muxer and get data");
            }
        }

        public void Dispose()
        {
            if (!_isClosed)
                CloseChunk();
        }

        private void PushData(IntPtr data, int size, DateTime timeStamp, bool isKeyFrame)
        {
            _muxerContext.Push(data, size, timeStamp, isKeyFrame);
        }
    }
}