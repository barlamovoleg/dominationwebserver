using System;
using System.Runtime.InteropServices;

namespace NetVideo.Codec
{
    internal static class UnsafeNativeMethods
    {
        // ReSharper disable once InconsistentNaming
        private const string KERNEL32 = "kernel32.dll";

        [DllImport(KERNEL32, EntryPoint = "CreateFileMappingW", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern IntPtr CreateFileMapping(
            IntPtr hFile,
            IntPtr lpFileMappingAttributes, 
            FileMapProtection flProtect,
            int dwMaximumSizeHigh,
            int dwMaximumSizeLow, 
            string lpName);

        [Flags]
        private enum FileMapProtection : uint
        {
            PageReadonly = 0x02,
            PageReadWrite = 0x04,
            PageWriteCopy = 0x08,
            PageExecuteRead = 0x20,
            PageExecuteReadWrite = 0x40,
            SectionCommit = 0x8000000,
            SectionImage = 0x1000000,
            SectionNoCache = 0x10000000,
            SectionReserve = 0x4000000,
        }

        [DllImport(KERNEL32, SetLastError = true, ExactSpelling = true)]
        private static extern IntPtr MapViewOfFile(
            IntPtr hFileMappingObject, 
            FileMapAccess dwDesiredAccess,
            uint dwFileOffsetHigh, 
            uint dwFileOffsetLow,
            UIntPtr dwNumberOfBytesToMap);
        
        public static IntPtr MapViewOfFile(IntPtr mapping, uint size)
        {
            return MapViewOfFile(mapping, FileMapAccess.FileMapAllAccess, 0u, 0u, new UIntPtr(size));
        }

        public static IntPtr CreateFileMapping(int size)
        {
            return CreateFileMapping(new IntPtr(-1), IntPtr.Zero, FileMapProtection.PageReadWrite, 0, size, null);
        }

        [Flags]
        private enum FileMapAccess : uint
        {
            FileMapCopy = 0x0001,
            FileMapWrite = 0x0002,
            FileMapRead = 0x0004,
            FileMapAllAccess = 0x001f,
            FileMapExecute = 0x0020,
        }

        [DllImport(KERNEL32, ExactSpelling = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool UnmapViewOfFile(IntPtr lpBaseAddress);

        [DllImport(KERNEL32, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool CloseHandle(IntPtr handle);
    }
}