﻿using System.IO;
using Core;

namespace Muxers
{
    internal interface IMuxerWithChunks : IVideoMuxer
    {
        Stream GetChunkData(int index);
    }
}
