﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Annotations;
using Core;
using Core.Config;

namespace ServerLogic
{
    public class AllServersConnector
    {
        private IReadOnlyList<IServerConnector> ServerConnectors { get; }
        private ServersController ServersController { get; }

        public AllServersConnector([NotNull] IEnumerable<IServerConnector> serverConnectors, [NotNull] ServersController serversController)
        {
            if (serverConnectors == null) throw new ArgumentNullException(nameof(serverConnectors));

            ServerConnectors = serverConnectors.ToList();
            ServersController = serversController ?? throw new ArgumentNullException(nameof(serversController));
        }

        public Task ConnectServersAsync(IReadOnlyList<ServerInfo> servers, CancellationToken cancellationToken)
        {
            return ServersController.ConnectServersAsync(servers, ServerConnectors, cancellationToken);
        }
    }
}
