﻿import { Component } from '@angular/core';
import { VideoGridsService } from "../../services/video-grids-service";
import { GridType, VideoGrid } from "../../models/gridTypes";

@Component({
    selector: 'app-video-grid',
    templateUrl: './video-grid.component.html',
    styleUrls: ['./video-grid.component.scss']
})
export class VideoGridComponent {

    private get selectedVideoGrid(): VideoGrid {
        return this.videoGridsService.selectedVideoGrid;
    }

    constructor(private videoGridsService: VideoGridsService) {
        
    }
}