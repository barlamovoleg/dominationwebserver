﻿import { Channel } from "../models/channels";
import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()

export class ChannelsProvider {

    private url = "/api/channels";

    private channels: Channel[];

    constructor(private http: Http) {
        this.channels = [];
    }

    public getChannels(): Channel[] {
        this.preLoadIfNeed();

        return this.channels;
    }

    public preLoadIfNeed() {
        if (this.channels.length < 1) {
            this.channels = this.receiveChannels();
        }
    }

    private receiveChannels(): Channel[] {

        var response = this.http.get(this.url);
        response.subscribe((response) => {
            var json = response.json();
            this.channels = json as Channel[];
        });
        return this.channels;
    }

    //private getFakeChannels(): Channel[] {
    //    return [
    //        new Channel("root1", "", ""),
    //        new Channel("root2", "", ""),
    //        new Channel("root3", "", ""),
    //        new Channel("sub1", "folder1", ""),
    //        new Channel("sub2", "folder1", ""),
    //        new Channel("sub3", "folder1", ""),

    //        new Channel("sub21", "folder2", ""),
    //        new Channel("sub22", "folder2", ""),
    //        new Channel("sub23", "folder2", ""),

    //        new Channel("sub221", "folder2/subfolder", ""),
    //        new Channel("sub222", "folder2/subfolder", ""),
    //        new Channel("sub223", "folder2/subfolder", "")
    //    ];
    //}

}