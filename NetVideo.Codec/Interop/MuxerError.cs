﻿namespace NetVideo.Codec.Interop
{
    public enum MuxerError
    {
        Success = 0,

        MuxerTypeNotSet = 1,
        MuxerOpenError = 2,

        MustOpenBeforeError = 3,
        WriteHeadersError = 4,
        WriteDataError = 5,
    }
}
