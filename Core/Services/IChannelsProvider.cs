﻿using System;
using System.Collections.Generic;
using Common.Annotations;

namespace Core.Services
{
    public interface IChannelsProvider
    {
        IReadOnlyList<IChannel> GetChannels();
        [CanBeNull] IChannel FindChannelByID(int channelID);
        [CanBeNull] IChannel FindChannelByMarker(string channelMarker);
    }
}
