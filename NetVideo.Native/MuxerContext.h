#pragma once

#include "stdafx.h"

#include "MuxerError.h"

struct IOOutput {
	uint8_t* outBuffer;
	int bytesSet;
};

struct buffer_data {
	uint8_t * buf;
	size_t size;
	uint8_t * ptr;
	size_t room; ///< size left in the buffer

};

class MuxerContext
{
private:
	AVFormatContext* _context = nullptr;
	AVStream* _stream = nullptr;
	AVIOContext* _avioContext = nullptr;


	uint8_t *_avio_ctx_buffer = NULL;
	size_t avio_ctx_buffer_size = 4096;
	struct buffer_data bd = { 0 };
	const size_t bd_buf_size = 1024;

public:
	explicit MuxerContext();
	virtual ~MuxerContext();

	MuxerError Open(AVOutputFormat* muxerType);

	MuxerError Push(uint8_t data[], const int32_t size, const long timestamp, bool isKeyFrame);

	MuxerError Close(uint8_t outputData[]) const;

	const int GetLength();
};

