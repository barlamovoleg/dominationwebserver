﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core
{
    public interface IChannel : IDisposable
    {
        IServer ServerOwner { get; }

        bool IsHidden { get; }

        int ChannelID { get; }
        string Name { get; }

        string Hierarchy { get; }

        Task<IRawVideoSource> GetVideoSourceAsync(CancellationToken cancellationToken);

        string GetMarker();
    }
}
