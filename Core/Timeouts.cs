﻿using System;

namespace Core
{
    public static class Timeouts
    {
#if DEBUG
        public static TimeSpan LoadAppDefault => TimeSpan.FromSeconds(60 * 2);
#else
        public static TimeSpan LoadAppDefault => TimeSpan.FromSeconds(10);
#endif


    }
}
