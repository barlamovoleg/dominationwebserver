﻿import { Component, OnInit, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { PLATFORM_ID } from '@angular/core';

@Component({
    selector: 'app-clock',
    templateUrl: './clock.component.html',
    styleUrls: ['./clock.component.scss']
})
export class ClockComponent implements OnInit {
    constructor(@Inject(PLATFORM_ID) private platformId: Object) {
        this.refreshDate();
    }

    public now: Date = new Date();

    ngOnInit() {
        // setting up the refresh interval caused a timeout in prerendering, so only set up interval if rendering in browser
        if (isPlatformBrowser(this.platformId)) {
            this.setUpRefreshInterval();
        }
    }

    private setUpRefreshInterval(): void {
        var self = this;
        setInterval(function() {
            self.refreshDate();
        }, 1000);
    }

    private refreshDate(): void {
        this.now = new Date();
    }
}