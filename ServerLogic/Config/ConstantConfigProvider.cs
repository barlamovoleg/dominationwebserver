﻿using System;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.Config;
using Core.Services;

namespace ServerLogic.Config
{
    internal class ConstantConfigProvider : IConfigProvider
    {
        private WebServerConfig _loadedConfig;

        public Task<WebServerConfig> GetConfigAsync(CancellationToken cancellationToken)
        {
            if (_loadedConfig == null)
                _loadedConfig = new ConstantWebConfig();

            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(_loadedConfig);
        }

        [Serializable]
        [DataContract(Namespace = "")]
        public class ConstantWebConfig : WebServerConfig
        {
            public ConstantWebConfig()
            {
                var serverInfo = new ServerInfo
                {
                    TargetServerType = "DominationServer",
                    AuthInfo = new AuthInfo("developer", "7777777"),
                    ServerHost = "192.168.0.17",
                    ServerPort = 7010
                };

                ServersInfos.Add(serverInfo);
            }
        }
    }
}
