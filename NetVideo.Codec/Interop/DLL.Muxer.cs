﻿using System;
using System.Runtime.InteropServices;
using NetVideo.Codec.Muxing;

namespace NetVideo.Codec.Interop
{
    internal static partial class NativeMethods
    {
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateMuxerContext();

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr FindMuxer(MuxerType muxerType);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr FindMuxerByName(string muxerName);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern MuxerError OpenMuxer(IntPtr context, MuxerType muxerType);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern MuxerError Push(IntPtr context, IntPtr data, int size, long timeStamp, bool isKeyFrame);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        //TODO copy data!!! BAD!
        public static extern MuxerError Close(IntPtr muxer, IntPtr outputData);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void DeleteMuxerContext(IntPtr context);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetLength(IntPtr context);
    }
}
