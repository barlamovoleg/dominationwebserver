﻿import { Component, Input, AfterContentInit } from '@angular/core';
import { ITreeNode } from "../../models/tree";

@Component({
    selector: 'app-treeview',
    templateUrl: './treeview.component.html',
    styleUrls: ['./treeview.component.scss']
})
export class TreeviewComponent implements AfterContentInit {

    @Input() nodes: Array<ITreeNode>;

    @Input() private currentNode: ITreeNode | null;

    private allowDrag : boolean;

    constructor() {
        this.nodes = new Array<ITreeNode>(0);
        this.currentNode = null;
        this.allowDrag = true;
    }

    ngAfterContentInit(): void {
        if (navigator) {
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                this.allowDrag = true;
            }
        }
    }

    private canDragNode(event: DragEvent, node: ITreeNode | null): boolean {

        var canDrag = node != null && node.canDrag();
        if (!canDrag)
            return false;
        else {
            var notNullNode = node as ITreeNode;
            if (notNullNode == null)
                return false;

            event.dataTransfer.effectAllowed = 'copy';
            var dataContext = JSON.stringify(notNullNode.context);
            //event.dataTransfer.setData("tree_node", dataContext);
            event.dataTransfer.setData("text", dataContext);

            return true;
        }
    }

    iconExist(node: ITreeNode): boolean {
        return node.pathToIcon !== "";
    }

    onExpand(node: ITreeNode) {

        node.isExpanded = !node.isExpanded;
    }
}