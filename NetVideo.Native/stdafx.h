// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

#include <cstdio>
#include <cstdlib>
#include <cstdint>

extern "C"
{
	#ifndef __STDC_CONSTANT_MACROS
		#define __STDC_CONSTANT_MACROS
	#endif

	#include <libavcodec/avcodec.h>
	#include <libavformat/avformat.h>
	#include <libavutil/opt.h>
	#include <libavutil/imgutils.h>
	#include <libswscale/swscale.h>
}

// libav
#if defined(_M_IX86)
	#pragma comment(lib, "..\\ffmpeg\\lib_win32\\avcodec.lib")
	#pragma comment(lib, "..\\ffmpeg\\lib_win32\\avformat.lib")
	#pragma comment(lib, "..\\ffmpeg\\lib_win32\\avutil.lib")
	#pragma comment(lib, "..\\ffmpeg\\lib_win32\\swscale.lib")
	#pragma comment(lib, "..\\ffmpeg\\lib_win32\\swresample.lib")
#endif

#if defined(_M_X64)
	#pragma comment(lib, "..\\ffmpeg\\lib_win64\\avcodec.lib")
	#pragma comment(lib, "..\\ffmpeg\\lib_win64\\avformat.lib")
	#pragma comment(lib, "..\\ffmpeg\\lib_win64\\avutil.lib")
	#pragma comment(lib, "..\\ffmpeg\\lib_win64\\swscale.lib")
	#pragma comment(lib, "..\\ffmpeg\\lib_win64\\swresample.lib")
#endif
