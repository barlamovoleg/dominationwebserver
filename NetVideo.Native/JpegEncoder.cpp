#include "stdafx.h"
#include "JpegEncoder.h"

JpegEncoder::JpegEncoder()
{
}


JpegEncoder::~JpegEncoder()
{
}


int frame_to_jpeg(AVFrame *frame)
{
	AVCodec *jpegCodec = avcodec_find_encoder(AV_CODEC_ID_JPEG2000);
	if (jpegCodec == nullptr) {
		return -1;
	}

	AVCodecContext *jpegContext = avcodec_alloc_context3(jpegCodec);
	if (jpegContext == nullptr) {
		return -1;
	}

	AVFrame* fff = av_frame_alloc();
	if (fff == nullptr) {
		return -1;
	}

	jpegContext->pix_fmt = static_cast<AVPixelFormat>(frame->format);
	jpegContext->height = frame->height;
	jpegContext->width = frame->width;
	//jpegContext->sample_aspect_ratio = frame->sample_aspect_ratio;
	//jpegContext->time_base = video_dec_ctx->time_base;
	//jpegContext->compression_level = 100;
	//jpegContext->thread_count = 1;
	//jpegContext->prediction_method = 1;
	//jpegContext->flags2 = 0;
	//jpegContext->rc_max_rate = jpegContext->rc_min_rate = jpegContext->bit_rate = 80000000;

	if (avcodec_open2(jpegContext, jpegCodec, nullptr) < 0) {
		return -1;
	}

	int send = avcodec_send_frame(jpegContext, frame);

	AVPacket packet;
	av_init_packet(&packet);
	int recv = avcodec_receive_packet(jpegContext, &packet);

	av_packet_unref(&packet);
	//av_free_packet(&packet);
	avcodec_free_context(&jpegContext);

	return 0;
}