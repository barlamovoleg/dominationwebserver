﻿using System;
using System.Runtime.Serialization;
using Common.Annotations;

// ReSharper disable once CheckNamespace
namespace Core
{
    [Serializable]
    public class AuthInfo
    {
        [DataMember]
        public string Login { get; }

        [DataMember]
        public string Password { get; }

        public AuthInfo([NotNull] string login, [NotNull] string password)
        {
            if (string.IsNullOrWhiteSpace(login)) throw new ArgumentException(nameof(login));
            Login = login ?? throw new ArgumentNullException(nameof(login));
            Password = password ?? throw new ArgumentNullException(nameof(password));
        }
    }
}
