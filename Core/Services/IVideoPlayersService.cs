﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Services
{
    public interface IVideoPlayersService : IDisposable
    {
        Task<IVideoPlayer> GetOrCreatePlayerAsync(IChannel channel, CancellationToken cancellationToken);

        void DestroyIfExists(IVideoPlayer videoPlayer);
    }
}
