﻿using System;
using System.Runtime.InteropServices;
using Core.Messages;
using Microsoft.Win32.SafeHandles;

namespace Core
{
    public sealed class SafeNativeMemoryHandle : SafeHandleZeroOrMinusOneIsInvalid, IFrameData
    {
        public int Size { get; }
        public bool IsEmpty { get; }

        public SafeNativeMemoryHandle(int size) : base(true)
        {
            Size = size;
            IsEmpty = Size < 1;

            SetHandle(Marshal.AllocHGlobal(size));

            if (!IsEmpty)
                GC.AddMemoryPressure(Size);
        }

        protected override bool ReleaseHandle()
        {
            if (handle == IntPtr.Zero)
                return false;

            if (!IsEmpty)
                GC.RemoveMemoryPressure(Size);

            Marshal.FreeHGlobal(handle);
            handle = IntPtr.Zero;
            return true;
        }
    }
}
