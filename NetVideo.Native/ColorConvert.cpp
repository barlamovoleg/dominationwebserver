#include "stdafx.h"
#include "ColorConvert.h"

int32_t ColorConvert::Convert(ConversionParams* params)
{
	const auto srcData = params->srcData;
	const auto srcStride = params->srcStride;
	const int32_t srcWidth = params->srcWidth;
	const int32_t srcHeight = params->srcHeight;
	const AVPixelFormat srcFormat = FixYUVJ(params->srcFormat);

	const auto dstData = params->dstData;
	const auto dstStride = params->dstStride;
	const int32_t dstWidth = params->dstWidth > 0 ? params->dstWidth : srcWidth;
	const int32_t dstHeight = params->dstHeight > 0 ? params->dstHeight : srcHeight;
	const AVPixelFormat dstFormat = params->dstFormat != AV_PIX_FMT_NONE ? FixYUVJ(params->dstFormat) : srcFormat;

	if (params->invertYUV420Planes)
	{
		if (srcFormat == AV_PIX_FMT_YUV420P)
		{
			InvertYuvPlane(srcData[1], srcStride[1] * srcHeight / 2);
			InvertYuvPlane(srcData[2], srcStride[2] * srcHeight / 2);
		}
		else if (srcFormat == AV_PIX_FMT_NV12 || srcFormat == AV_PIX_FMT_NV21)
		{
			InvertYuvPlane(srcData[1], srcStride[1] * srcHeight / 2);
		}
	}

    // errors(measured in sum - squared - differences)
    // SWS_POINT   SWS_FAST_BILINEAR  SWS_BILINEAR  SWS_BICUBLIN  SWS_BICUBIC  SWS_SPLINE  SWS_SINC
    // 3769328057  2636583475         2155004550    2039237301    1980996866   1951467852  1902084412
    // 
    // time(how much time to downsample, copy and upsample, in seconds)
    // SWS_POINT  SWS_FAST_BILINEAR   SWS_BILINEAR  SWS_BICUBLIN  SWS_BICUBIC  SWS_SPLINE  SWS_SINC
    // 1.1034     1.2289              1.7151        2.2899        2.6026       6.719       13.2753

	SwsContext* conversion = sws_getContext(
		srcWidth, srcHeight, srcFormat,
		dstWidth, dstHeight, dstFormat,
		SWS_BILINEAR, nullptr, nullptr, nullptr);

	const int32_t outputHeight = sws_scale(conversion,
		srcData, srcStride,
		0, srcHeight,
		dstData, dstStride);

	sws_freeContext(conversion);

	return outputHeight;
}

void ColorConvert::InvertYuvPlane(uint8_t plane[], const int32_t planeSize)
{
	for (auto i = 0; i < planeSize; i++)
		plane[i] = ~plane[i];
}

AVPixelFormat ColorConvert::FixYUVJ(const AVPixelFormat decodedFormat)
{
	switch (decodedFormat)
	{
	case AV_PIX_FMT_YUVJ420P: return AV_PIX_FMT_YUV420P;
	case AV_PIX_FMT_YUVJ422P: return AV_PIX_FMT_YUV422P;
	case AV_PIX_FMT_YUVJ444P: return AV_PIX_FMT_YUV444P;
	case AV_PIX_FMT_YUVJ440P: return AV_PIX_FMT_YUV440P;
	default: return decodedFormat;
	}
}