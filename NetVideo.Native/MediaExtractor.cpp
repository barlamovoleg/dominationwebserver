#include "stdafx.h"
#include "MediaExtractor.h"

const double eps_zero = 0.000025;

double r2d(const AVRational r)
{
    return r.num == 0 || r.den == 0 ? 0.0 : (double)r.num / (double)r.den;
}

MediaExtractor::MediaExtractor()
    : format_context(nullptr)
{

}

int32_t MediaExtractor::Initialize(const char* filename)
{ 
    // Open video file
    int32_t err = avformat_open_input(&format_context, filename, nullptr, nullptr);
    if (err < 0)
    {
        fprintf(stderr, "ffmpeg: Unable to open input file\n");
        return -1;
    }

    // Retrieve stream information
    err = avformat_find_stream_info(format_context, nullptr);
    if (err < 0)
    {
        fprintf(stderr, "ffmpeg: Unable to find stream info\n");
        return -2;
    }

    return EXIT_SUCCESS;
}

double MediaExtractor::GetFps(int32_t streamIndex) const
{
    const auto fps = r2d(av_guess_frame_rate(format_context, format_context->streams[streamIndex], nullptr));
    return fps;
}

double MediaExtractor::GetTotalDurationSec(int32_t streamIndex) const
{                      
    double sec = (double)format_context->duration / (double)AV_TIME_BASE;

    if (sec < eps_zero)
    {
        const auto stream = format_context->streams[streamIndex];
        sec = (double)stream->duration * r2d(stream->time_base);
    }

    return sec;
}

int64_t MediaExtractor::GetFrameCount(int32_t streamIndex) const
{
    int64_t nbf = format_context->streams[streamIndex]->nb_frames;
    if (nbf == 0)
    {                       
        nbf = (int64_t)floor(GetTotalDurationSec(streamIndex) * GetFps(streamIndex) + 0.5);          
    }

    return nbf;
}

void MediaExtractor::DumpInformation(const char* filename) const
{
    // Dump information about file onto standard error
    av_dump_format(format_context, 0, filename, 0);
}

int32_t MediaExtractor::FindStream(const AVMediaType mediaType, AVCodecID& codecType) const
{
    codecType = AVCodecID::AV_CODEC_ID_NONE;

    uint32_t stream;
    for (stream = 0; stream < format_context->nb_streams; ++stream)
        if (format_context->streams[stream]->codecpar->codec_type == mediaType)
            break;

    if (stream == format_context->nb_streams)
        return -1;

    AVCodecParameters* parameters = format_context->streams[stream]->codecpar;
    codecType = parameters->codec_id;

    return stream;
}

AVCodecParameters* MediaExtractor::GetCodecParameters(int32_t streamIndex) const
{
    const auto parameters = format_context->streams[streamIndex]->codecpar;
    return parameters;
}

bool MediaExtractor::ReadFrame(AVPacket* packet) const
{
    const auto result = av_read_frame(format_context, packet);
    return result >= 0;
}

bool MediaExtractor::Seek(int32_t streamIndex, double seekSec) const
{
    const auto stream = format_context->streams[streamIndex];
    const int64_t seek_ts = (seekSec/r2d(stream->time_base));

    return av_seek_frame(format_context, streamIndex, seek_ts, AVSEEK_FLAG_BACKWARD) >= 0;
}

MediaExtractor::~MediaExtractor()
{
    // Close the video file
    if (format_context != nullptr)
        avformat_close_input(&format_context);
}
