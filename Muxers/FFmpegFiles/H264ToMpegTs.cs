﻿//using System;
//using System.IO;
//using System.Threading;
//using System.Threading.Channels;
//using System.Threading.Tasks;
//using Common.Annotations;
//using Core;
//using Core.Messages;
//using Core.Services;

//namespace Muxers.FFmpegFiles
//{
//    internal class H264ToMpegTs : IVideoMuxer
//    {
//        public IEnvironmentService EnvironmentService { get; }

//        public ChannelReader<byte[]> OutputVideo => _outputChannel.Reader;

//        private ChannelWriter<byte[]> OutputWriter => _outputChannel.Writer;

//        private readonly Channel<byte[]> _outputChannel = Channel.CreateUnbounded<byte[]>(new UnboundedChannelOptions
//        {
//            SingleReader = false,
//            SingleWriter = true,
//            AllowSynchronousContinuations = false
//        });

//        private FFmpegWrapperProcess _fFmpegWrapperProcess;

//        private readonly string _pathToFfmpegExe;
//        private const string FFmpegExeFileName = "ffmpeg.exe";
//        private const int BytesBufferLength = 4096;
//        private readonly string _workingFolderPath;
//        private readonly string ID = Guid.NewGuid().ToString("N");
//        private readonly string _outputFilePath;

//        private CancellationTokenSource _muxingCancellationTokenSource;

//        public H264ToMpegTs([NotNull] IEnvironmentService environmentService)
//        {
//            EnvironmentService = environmentService ?? throw new ArgumentNullException(nameof(environmentService));

//            _pathToFfmpegExe = Path.Combine(EnvironmentService.LibsPath, FFmpegExeFileName);
//            _workingFolderPath = Path.Combine(EnvironmentService.TempFolderPath, ID);
//            _outputFilePath = Path.Combine(_workingFolderPath, "out.m3u8");
//        }

//        public void StartMuxing(ChannelReader<VideoMessage> inputRawVideo)
//        {
//            StopMuxing();

//            Directory.CreateDirectory(_workingFolderPath);

//            _fFmpegWrapperProcess = new FFmpegWrapperProcess(_pathToFfmpegExe,
//                $"-re -i - -f h264 -r 25 -c:v copy -bufsize {BytesBufferLength}k -f hls -flags -global_header -hls_time 2 -hls_list_size 4 -hls_flags delete_segments -start_number 1 -hls_allow_cache 0 -y \"{_outputFilePath}\"");

//            _muxingCancellationTokenSource = new CancellationTokenSource();
//            ReadWriteAsync(inputRawVideo, _muxingCancellationTokenSource.Token).GetAwaiter();

//            //TODO 
//            //await WaitUntilFileNotExists(_outputFilePath, cancellationToken).ConfigureAwait(false);
//        }

//        public void StopMuxing()
//        {
//            if (_muxingCancellationTokenSource != null)
//            {
//                _muxingCancellationTokenSource.Cancel();
//                _muxingCancellationTokenSource.Dispose();
//            }
//        }

//        private async Task ReadWriteAsync(ChannelReader<VideoMessage> inputRawVideo, CancellationToken cancellationToken)
//        {
//            await Task.Delay(1, cancellationToken).ConfigureAwait(false);

//            var bytesBuffer = new byte[BytesBufferLength];

//            while (true)
//            {
//                cancellationToken.ThrowIfCancellationRequested();

//                //TODO
//                //var readedBytesCount = await inputRawVideo.ReadAsync(
//                //    bytesBuffer, 0, BytesBufferLength, cancellationToken).ConfigureAwait(false);

//                cancellationToken.ThrowIfCancellationRequested();

//                //if (readedBytesCount <= 0)
//                //{
//                //    inputRawVideo.Position = 0;
//                //    await Task.Delay(100, cancellationToken).ConfigureAwait(false);
//                //    continue;
//                //}

//                //await _fFmpegWrapperProcess.WriteToFFmpegAsync(bytesBuffer, 0, readedBytesCount, cancellationToken)
//                //    .ConfigureAwait(false);
//            }
//        }

//        private Task WaitUntilFileNotExists(string filePath, CancellationToken cancellationToken)
//        {
//            return Task.Run(async () =>
//            {
//                await Task.Delay(1, cancellationToken).ConfigureAwait(false);

//                while (true)
//                {
//                    try
//                    {
//                        if (File.Exists(filePath))
//                            return;

//                        await Task.Delay(100, cancellationToken).ConfigureAwait(false);
//                    }
//                    catch (Exception)
//                    {
//                        // ignored
//                    }
//                }
//            }, cancellationToken);
//        }

//        public void Dispose()
//        {
//            _fFmpegWrapperProcess.Dispose();
//        }
//    }
//}