﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Core.Messages;

namespace Core
{
    public interface IVideoMuxer : IDisposable
    {
        Stream OutputStream { get; }

        void StartMuxing(ChannelReader<VideoMessage> inputRawVideo);

        void StopMuxing();

        Task WaitFirstChunkAsync(CancellationToken cancellationToken);
    }
}
