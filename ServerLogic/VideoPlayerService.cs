﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Common.Annotations;
using Core;
using Core.Logging;
using Core.Services;

namespace ServerLogic
{
    internal class VideoPlayerService : IVideoPlayersService
    {
        public ReadOnlyCollection<IVideoPlayer> VideoPlayers { get; }

        private IChannelsProvider ChannelsProvider { get; }
        private IVideoPlayersFactory VideoPlayersFactory { get; }
        private IVideoPlayersLifeTimesService LifeTimesService { get; }

        private bool _isDisposed;

        private readonly ConcurrentDictionary<IChannel, Semaphore> _waitHandles = new ConcurrentDictionary<IChannel, Semaphore>();
        private readonly ObservableCollection<IVideoPlayer> _videoPlayers = new ObservableCollection<IVideoPlayer>();
        private readonly object _videoPlayersLocker = new object();

        public VideoPlayerService([NotNull] IChannelsProvider channelsProvider, [NotNull] IVideoPlayersFactory videoPlayersFactory,
            [NotNull] IVideoPlayersLifeTimesService lifeTimesService)
        {
            ChannelsProvider = channelsProvider ?? throw new ArgumentNullException(nameof(channelsProvider));
            VideoPlayersFactory = videoPlayersFactory ?? throw new ArgumentNullException(nameof(videoPlayersFactory));
            LifeTimesService = lifeTimesService ?? throw new ArgumentNullException(nameof(lifeTimesService));
            VideoPlayers = new ReadOnlyCollection<IVideoPlayer>(_videoPlayers);

            LifeTimesService.PlayerLifeTimeExpired += LifeTimesServiceOnPlayerLifeTimeExpired;
        }

        public async Task<IVideoPlayer> GetOrCreatePlayerAsync(IChannel channel, CancellationToken cancellationToken)
        {
            if (channel == null) throw new ArgumentNullException(nameof(channel));
            if (_isDisposed) throw new ObjectDisposedException(nameof(VideoPlayerService));

            //чтобы запросы по разным каналм не блокировали друг друга.
            var waitHandle = _waitHandles.GetOrAdd(channel, channel1 => new Semaphore(1, 1));
            await waitHandle.WaitOneAsync(cancellationToken).ConfigureAwait(false);

            try
            {
                IVideoPlayer existsPlayer;

                lock (_videoPlayersLocker)
                {
                    existsPlayer = _videoPlayers.FirstOrDefault(player => player.ChannelMatch(channel));
                }

                if (existsPlayer != null)
                {
                    LifeTimesService.ResetLifeTime(existsPlayer);
                    return existsPlayer;
                }

                var newPlayer = await CreateVideoPlayerAsync(channel, cancellationToken).ConfigureAwait(false);

                lock (_videoPlayersLocker)
                {
                    _videoPlayers.Add(newPlayer);
                }

                LifeTimesService.CreateLifeTime(newPlayer);

                return newPlayer;
            }
            finally
            {
                waitHandle.Release(1);
            }
        }

        public void DestroyIfExists([NotNull] IVideoPlayer videoPlayer)
        {
            if (videoPlayer == null) throw new ArgumentNullException(nameof(videoPlayer));

            lock (_videoPlayersLocker)
            {
                _videoPlayers.Remove(videoPlayer);
            }

            videoPlayer.Dispose();
        }

        public void Dispose()
        {
            _isDisposed = true;

            LifeTimesService.PlayerLifeTimeExpired -= LifeTimesServiceOnPlayerLifeTimeExpired;

            var collection = _waitHandles.Values.ToArray();
            _waitHandles.Clear();

            foreach (var waitHandle in collection)
            {
                try
                {
                    waitHandle.Dispose();
                }
                catch (Exception e)
                {
                    Logger.Log(e, "Error while destroy channel waithandle");
                }
            }

            IReadOnlyList<IVideoPlayer> players;

            lock (_videoPlayersLocker)
            {
                players = _videoPlayers.ToArray();
                _videoPlayers.Clear();
            }

            foreach (var videoPlayer in players)
            {
                videoPlayer.Dispose();
            }
        }

        private void LifeTimesServiceOnPlayerLifeTimeExpired(object sender, IVideoPlayer player)
        {
            if (_isDisposed)
                return;

            lock (_videoPlayersLocker)
            {
                _videoPlayers.Remove(player);
            }

            player.Dispose();
        }

        private async Task<IVideoPlayer> CreateVideoPlayerAsync([NotNull] IChannel channel, CancellationToken cancellationToken)
        {
            if (channel == null) throw new ArgumentNullException(nameof(channel));

            var videoSource = await channel.GetVideoSourceAsync(cancellationToken).ConfigureAwait(false);
            return await VideoPlayersFactory.CreateVideoPlayerAsync(videoSource, channel, cancellationToken).ConfigureAwait(false);
        }
    }
}