﻿//using System;
//using System.Threading;
//using System.Threading.Tasks;
//using Common.Annotations;
//using Core;
//using Core.Services;

//namespace Muxers.FFmpegFiles
//{
//    internal class FFmpegFilesMuxersFactory : IVideoMuxersFactory
//    {
//        [NotNull] private IEnvironmentService EnvironmentService { get; }

//        public FFmpegFilesMuxersFactory([NotNull] IEnvironmentService environmentService)
//        {
//            EnvironmentService = environmentService ?? throw new ArgumentNullException(nameof(environmentService));
//        }

//        public Task<IVideoMuxer> CreateAsync(CancellationToken cancellationToken)
//        {
//            return Task.FromResult((IVideoMuxer)new H264ToMpegTs(EnvironmentService));
//        }
//    }
//}
