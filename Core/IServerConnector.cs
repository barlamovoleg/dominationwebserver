﻿using System.Threading;
using System.Threading.Tasks;
using Core.Config;

namespace Core
{
    public interface IServerConnector
    {
        string TargetServerType { get; }
        Task<IServer> ConnectToServerAsync(ServerInfo serverInfo, CancellationToken cancellationToken);
    }
}
