﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Muxers.FFmpegFiles
{
    class FFmpegWrapperProcess : IDisposable
    {
        private readonly Process _ffmpegProcess;

        public FFmpegWrapperProcess(string pathToFFmpeg, string args)
        {
            var processStartInfo = new ProcessStartInfo(pathToFFmpeg)
            {
                Arguments = args,
                RedirectStandardInput = true,
                UseShellExecute = false,
                CreateNoWindow = false,
                WindowStyle = ProcessWindowStyle.Normal,
            };

            _ffmpegProcess = Process.Start(processStartInfo);
        }

        public Task WriteToFFmpegAsync(byte[] bytes, int offset, int bytesCount, CancellationToken cancellationToken)
        {
            return _ffmpegProcess.StandardInput.BaseStream.WriteAsync(bytes, offset, bytesCount, cancellationToken);
        }

        public void Dispose()
        {
            _ffmpegProcess.Kill();
        }
    }
}
