#pragma once

enum class MuxerType
{
	Unknown = 0,

	Mpegts = 1,
};

inline const char* ToMuxerName(MuxerType muxerType)
{
	switch (muxerType)
	{
	case MuxerType::Mpegts: return "mpegts";
	default: return "";
	}
}