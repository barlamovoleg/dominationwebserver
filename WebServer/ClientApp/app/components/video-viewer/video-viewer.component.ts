﻿import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { Channel } from "../../models/channels";
import { Http } from "@angular/http";

declare function videojs(id: any, options: any, ready: any): any;

@Component({
    selector: 'app-video-viewer',
    templateUrl: './video-viewer.component.html',
    styleUrls: ['./video-viewer.component.scss']
})
export class VideoViewerComponent {

    @Input() public channel: Channel | undefined;

    private player: any; 

    private playerContainer: HTMLElement | null = null;

    private videoContainerId : string;

    private videoId: string;

    private urlApi = "/api/hls";

    private initialized: boolean = false;

    private videoSource: any = false;

    private selectedVideoStream: VideoStream = VideoStream.Auto;

    private lastVideoTime: Date | undefined = undefined;

    private videoResurrectionTimer : any;

    constructor(private http: Http) {
        this.videoId = this.guidGenerator();
        this.videoContainerId = 'video_' + this.videoId;
    }

    ngOnInit() {
        this.videoSource = this.getVideoSource();
    }

    ngAfterViewInit() {

        this.playerContainer = document.getElementById(this.videoContainerId);

        this.reinitializePlayer();

        this.initialized = true;
    }

    ngOnChanges(changes: { [propName: string]: SimpleChange }) {
        if (!this.initialized) return;
        if (changes['channel'] && changes['channel'].previousValue !== changes['channel'].currentValue) {
            this.videoSource = this.getVideoSource();

            this.onVideoSourceChanged();
        }
    }

    ngOnDestroy(): void {
        if (this.videoResurrectionTimer != undefined) {
            clearInterval(this.videoResurrectionTimer);
        }
        if (this.player != null)
            this.player.dispose();
    }

    public switchFullScreenMode() {
        var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
            (document.webkitFullscreenElement && document.webkitFullscreenElement !== null);

        if (!isInFullScreen) {

            this.player.requestFullscreen();
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }

            var self = this;
            setTimeout(function() { self.player.play(); }, 1000);
        }

        var thisElement = this;
        setTimeout(function() { thisElement.onVideoStreamChanged(); }, 1000);
    }

    public isInFullScreenMode() {
        return (document.fullscreenElement !== null && document.fullscreenElement != undefined)
            || (document.webkitFullscreenElement !== null && document.webkitFullscreenElement != undefined);
    }

    private enterFullScreen() {
        if (!this.isInFullScreenMode()) {
            this.switchFullScreenMode();
        }
    }

    private onVideoStreamChanged() {
        var lastVideoSource = this.videoSource;
        var newVideoSource = this.getVideoSource();

        if (lastVideoSource !== newVideoSource) {
            this.videoSource = newVideoSource;
            this.onVideoSourceChanged();
        }
    }

    private reinitializePlayer() {
        if (this.player != null)
            this.player.dispose();

        if (this.videoResurrectionTimer != undefined) {
            clearInterval(this.videoResurrectionTimer);
        }

        var self = this;

        // setup the player via the unique element ID
        this.player = videojs(this.playerContainer,
            {
                html: {
                    hls: {
                        overrideNative: true,
                    },
                    nativeAudioTracks: false,
                    nativeVideoTracks: false,
                    nativeTextTracks: false,
                },
            }, () => { });

        this.player.ready(() => {
            self.videoResurrectionTimer = setInterval(function() {

                var curTime = self.player.currentTime();

                if (self.lastVideoTime != undefined) {
                    if (curTime === self.lastVideoTime) {
                       self.onVideoSourceChanged();
                    }
                }

                self.lastVideoTime = curTime;

            }, 5000);
        });

        this.player.on('dblclick', () => {
            this.switchFullScreenMode();
        });

        this.player.on('error',
            () => {
                var context : any = this;
                if (context.error().code === 2) {
                    context.error(null);
                    context.pause();
                    context.load();
                    context.play();
                }
            });
    }

    private onVideoSourceChanged() {
        this.player.src({ type: 'application/x-mpegURL', src: this.videoSource });
    }

    private getVideoSource(): string {
        var channelNotNull = this.channel as Channel;
        if (channelNotNull == undefined) {
            return "";
        }

        var requestedStream = this.getStreamForRequest();
        var additional = requestedStream === VideoStream.Low ? "$alt" : "";

        return this.urlApi + "/" + channelNotNull.marker + additional;
    }

    private getStreamForRequest(): VideoStream {
        if (this.selectedVideoStream === VideoStream.Low)
            return VideoStream.Low;
        if (this.selectedVideoStream === VideoStream.Hight)
            return VideoStream.Hight;

        if (this.isInFullScreenMode())
            return VideoStream.Hight;
        else
            return VideoStream.Low;
    }

    private guidGenerator() : string {
        var S4 = () => (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }

    private showDropDown() {
        var menuElement = document.getElementById("quality-menu-" + this.videoContainerId);
        if (menuElement != null)
            menuElement.classList.add("popup-show");
    }

    private selectVideoStream(videoStream: VideoStream) {
        this.selectedVideoStream = videoStream;
        this.onVideoStreamChanged();
    }

}

enum VideoStream {
    Auto,
    Low,
    Hight,
}