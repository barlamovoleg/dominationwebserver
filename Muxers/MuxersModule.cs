﻿using Common.Annotations;
using Core;
using Core.Services;
using Microsoft.Extensions.DependencyInjection;
using Muxers.InMemoryHlsMuxer;

namespace Muxers
{
    [UsedImplicitly]
    public class MuxersModule : IServicesModule
    {
        public void RegisterServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<MuxersCollection>();
            serviceCollection.AddSingleton<IVideoMuxersFactory, InMemoryHlsMuxersFactory>();
        }
    }
}
