﻿using System;
using System.IO;

namespace Core
{
    public interface IVideoPlayer : IDisposable
    {
        string Name { get; }

        string ContentType { get; }

        Stream VideoStream { get; }

        bool ChannelMatch(IChannel channel);
    }
}
