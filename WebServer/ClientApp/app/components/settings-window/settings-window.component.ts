﻿import { Component, EventEmitter, Output, ElementRef, HostListener } from '@angular/core';

@Component({
    selector: 'app-settings-window',
    templateUrl: './settings-window.component.html',
    styleUrls: ['./settings-window.component.scss']
})

export class SettingsWindowComponent {
    constructor(private eRef: ElementRef) {
    }

    @Output() onClose = new EventEmitter();

    @HostListener('document:click', ['$event'])
    clickOut(event: any) {
        if (this.eRef.nativeElement.contains(event.target)) {
            //this.text = "clicked inside";
        } else {
            //this.onClose.emit();
        }
    }

    closeButtonClick() {
        this.onClose.emit();
    }

}