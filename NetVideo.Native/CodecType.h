#pragma once

#include <libavcodec/avcodec.h>

enum class CodecType
{
    Unknown = 0,

    H264 = 10,
    H265 = 11,

    MJPEG = 20,

    MPEG1 = 30,
    MPEG2 = 31,
    H261 = 32,
    H263 = 33,
    MPEG4 = 34,

    VP8 = 100,
    VP9 = 101,

    GSM = 1000,
    MuLaw = 1001,
    ALaw = 1002,

    G726_16k = 1003,
    G726_24k = 1004,
    G726_32k = 1005,
    G726_40k = 1006,


    G729 = 1007,
};

inline AVCodecID ToAVCodecID(CodecType codecType)
{
	switch (codecType)
	{
	case CodecType::H264:  return AVCodecID::AV_CODEC_ID_H264;
	case CodecType::H265:  return AVCodecID::AV_CODEC_ID_HEVC;
	case CodecType::MJPEG: return AVCodecID::AV_CODEC_ID_MJPEG;
	case CodecType::VP8:   return AVCodecID::AV_CODEC_ID_VP8;
	case CodecType::VP9:   return AVCodecID::AV_CODEC_ID_VP9;

	case CodecType::MPEG1: return AVCodecID::AV_CODEC_ID_MPEG1VIDEO;
	case CodecType::MPEG2: return AVCodecID::AV_CODEC_ID_MPEG2VIDEO;
	case CodecType::H261:  return AVCodecID::AV_CODEC_ID_H261;
	case CodecType::H263:  return AVCodecID::AV_CODEC_ID_H263;
	case CodecType::MPEG4: return AVCodecID::AV_CODEC_ID_MPEG4;

	case CodecType::GSM:   return AV_CODEC_ID_GSM;
	case CodecType::MuLaw: return AV_CODEC_ID_PCM_MULAW;
	case CodecType::ALaw:  return AV_CODEC_ID_PCM_ALAW;
	case CodecType::G726_16k:  return AV_CODEC_ID_ADPCM_G726;
	case CodecType::G726_24k:  return AV_CODEC_ID_ADPCM_G726;
	case CodecType::G726_32k:  return AV_CODEC_ID_ADPCM_G726;
	case CodecType::G726_40k:  return AV_CODEC_ID_ADPCM_G726;

    case CodecType::G729:  return AV_CODEC_ID_G729;

	default: return AVCodecID::AV_CODEC_ID_NONE;
	}
}

inline CodecType ToCodecType(AVCodecID codecID)
{
	switch (codecID)
	{
	case AVCodecID::AV_CODEC_ID_H264:       return CodecType::H264;
	case AVCodecID::AV_CODEC_ID_HEVC:       return CodecType::H265;
	case AVCodecID::AV_CODEC_ID_MJPEG:      return CodecType::MJPEG;
	case AVCodecID::AV_CODEC_ID_VP8:        return CodecType::VP8;
	case AVCodecID::AV_CODEC_ID_VP9:        return CodecType::VP9;
		
	case AVCodecID::AV_CODEC_ID_MPEG1VIDEO: return CodecType::MPEG1;
	case AVCodecID::AV_CODEC_ID_MPEG2VIDEO: return CodecType::MPEG2;
	case AVCodecID::AV_CODEC_ID_H261:       return CodecType::H261;
	case AVCodecID::AV_CODEC_ID_H263:       return CodecType::H263;
	case AVCodecID::AV_CODEC_ID_MPEG4:      return CodecType::MPEG4;

	case AVCodecID::AV_CODEC_ID_GSM:        return CodecType::GSM;
	case AVCodecID::AV_CODEC_ID_PCM_MULAW:  return CodecType::MuLaw;
	case AVCodecID::AV_CODEC_ID_PCM_ALAW:   return CodecType::ALaw;

	case AVCodecID::AV_CODEC_ID_ADPCM_G726: return CodecType::G726_32k; 

    case AVCodecID::AV_CODEC_ID_G729:       return CodecType::G729;

	default: return CodecType::Unknown;
	}
}