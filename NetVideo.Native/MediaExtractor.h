#pragma once

class MediaExtractor
{
private:
	AVFormatContext* format_context;

public:
	MediaExtractor();
	virtual ~MediaExtractor();

	int32_t Initialize(const char* filename);
    void DumpInformation(const char* filename) const;
    AVCodecParameters* GetCodecParameters(int32_t streamIndex) const;
    int32_t FindStream(AVMediaType mediaType, AVCodecID& codecType) const;
	bool ReadFrame(AVPacket* packet) const;
    bool Seek(int32_t streamIndex, double seekSec) const;
    double GetFps(int32_t streamIndex) const;
    int64_t GetFrameCount(int32_t streamIndex) const;
    double GetTotalDurationSec(int32_t streamIndex) const;
};
