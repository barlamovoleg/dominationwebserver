﻿import { Component, OnInit } from '@angular/core';
import { ChannelsProvider } from "../../services/channels-provider-service"
import { VideoGridsService } from "../../services/video-grids-service"
import { ITreeNode, TreeNode } from "../../models/tree";
import { Channel } from "../../models/channels";
import { GridCell } from "../../models/gridTypes";

@Component({
    selector: 'app-channels-tree',
    templateUrl: './channels-tree.component.html',
    styleUrls: ['./channels-tree.component.scss']
})
export class ChannelsTreeComponent implements OnInit {

    private rootNodes: ITreeNode[];
    private currentChannelsCount: Number = 0;

    constructor(private channelProvider: ChannelsProvider, private videoGridService: VideoGridsService) {
        this.rootNodes = [];
    }

    public onOpened() {
        var channels = this.channelProvider.getChannels();
        if (channels.length !== this.currentChannelsCount)
            this.reinitChannels();
    }

    ngOnInit() {
        this.reinitChannels();
    }

    private reinitChannels() {
        var rootCollections = this.getNodesCollections();

        this.rootNodes = [];
        this.fillNodesByCollectionRecursive(rootCollections, this.rootNodes);
    }

    private getNodesCollections(): NodeCollections {
        var rootCollections = new NodeCollections();
        var channels = this.channelProvider.getChannels();

        for (var channel of channels) {
            this.addChannelToTreeNodes(channel, rootCollections);
        }

        this.currentChannelsCount = channels.length;

        return rootCollections;
    }

    private fillNodesByCollectionRecursive(nodeCollection: NodeCollections, originalNodes: ITreeNode[]) {
        //order:
        //  folders..
        //      folders...
        //      channels...
        //  channels...
        //  ...

        for (var folderCollection of nodeCollection.folders) {
            if (folderCollection.node != null) {
                this.fillNodesByCollectionRecursive(folderCollection, folderCollection.node.children);
                originalNodes.push(folderCollection.node);
            }
        }

        for (var channelNode of nodeCollection.channels) {
            originalNodes.push(channelNode);
        }
    }

    private addChannelToTreeNodes(channel: Channel, collections: NodeCollections) {
        var location = channel.location;
        var targetFolderCollection = this.findOrCreateFolder(location, collections);

        var newChannelNode = this.createChannelNode(channel);

        targetFolderCollection.channels.push(newChannelNode);
    }

    private findOrCreateFolder(location: string, collections: NodeCollections): NodeCollections {
        if (location === "" || location == undefined || location == null)
            return collections;

        var locations = location.split("/");
        var currentCollections = collections;

        for (var locName of locations) {

            var existsFolder: any = this.findFolderInCollectionByName(currentCollections, locName);
            if (existsFolder == null) {
                existsFolder = this.addFolderToCollectionByName(currentCollections, locName);
            }

            currentCollections = existsFolder;
        }

        return currentCollections;
    }

    private findFolderInCollectionByName(collections: NodeCollections, folderName: string): NodeCollections | null {
        var folders = collections.folders;

        for (var folder of folders) {
            if (folder.node != null && folder.node.name === folderName)
                return folder;
        }

        return null;
    }

    private addFolderToCollectionByName(collections: NodeCollections, folderName: string): NodeCollections {
        var folderNode = this.createFolderNode(folderName);
        var newCollection = new NodeCollections();
        newCollection.node = folderNode;

        collections.folders.push(newCollection);
        return newCollection;
    }

    private createFolderNode(folderName: string): ITreeNode {
        var newFolderNode = new TreeNode(folderName);
        newFolderNode.pathToIcon = this.getFolderIconPath();
        newFolderNode.draggable = false;
        return newFolderNode;
    }

    private createChannelNode(channel: Channel): ITreeNode {
        var newChannelNode = new TreeNode(channel.name);
        newChannelNode.context = channel;
        newChannelNode.pathToIcon = this.getChannelIconPath(channel);
        newChannelNode.draggable = true;
        return newChannelNode;
    }

    private getChannelIconPath(channel: Channel): string {
        return "/dist/images/camera-item.svg";
    }

    private getFolderIconPath(): string {
        return "/dist/images/folder.svg";
    }

    private onDrop(event: DragEvent) {
        var canDrop = this.canDropData(event);
        if (!canDrop)
            return;

        if (this.onDropNode(event))
            return;
    }

    private onDropNode(event: DragEvent): boolean {
        var jsonData = event.dataTransfer.getData("text");
        if (jsonData == undefined || jsonData === "") {
            return false;
        }

        var nodeContext = JSON.parse(jsonData);
        var droppedCell = nodeContext as GridCell;
        if (droppedCell != null && droppedCell.channel != undefined) {
            this.onDropGridCell(droppedCell);
            return true;
        }

        return false;
    }

    private onDropGridCell(droppedCell: GridCell) {
        var index = droppedCell.index;
        var selectedVideoGrid = this.videoGridService.selectedVideoGrid;
        if (index < 0 || index >= selectedVideoGrid.cellsCount)
            return;

        var targetGridCell = selectedVideoGrid.findCellByIndex(index);
        if (targetGridCell == null)
            return;

        targetGridCell.channel = null;
    }

    private onDragOver(event: DragEvent) {
        var canDrop = this.canDropData(event);
        if (canDrop) {
            event.preventDefault();
        }
    }

    private canDropData(event: DragEvent) {
        //var isGridCellExists = event.dataTransfer.types.indexOf("grid_cell") >= 0;
        return true; //isGridCellExists;
    }
}

class NodeCollections {

    public node: ITreeNode | null;

    public folders: NodeCollections[];
    public channels: ITreeNode[];

    constructor() {
        this.folders = [];
        this.channels = [];
        this.node = null;
    }
}