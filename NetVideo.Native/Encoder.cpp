#include "stdafx.h"
#include "Encoder.h"
#include <mutex>

Encoder::Encoder(AVCodecID codecID) :
_encoder(nullptr), _context(nullptr), _frame(nullptr),
_format(nullptr), _formatContext(nullptr), _stream(nullptr), _streamBuffer(nullptr), muxStreamCallback(nullptr)
{
	_codecID = codecID;
}

Encoder::~Encoder()
{
	// ����������� ��� �������.
	CloseAll();
}

void Encoder::CloseAll()
{
	CloseEncoder();
	CloseStream();
}

EncoderError Encoder::OpenEncoder(int32_t width, int32_t height, int32_t fps, int32_t bitRate, int32_t gopSize)
{
	// �������������� ����������.
	_encoder = avcodec_find_encoder(_codecID);
	if (_encoder == nullptr)
		return EncoderError::FindEncoder;

	if (_encoder->id == AV_CODEC_ID_H264)
	{
		av_opt_set(_context->priv_data, "preset", "slow", 0);
		av_opt_set(_context->priv_data, "profile", "baseline", AV_OPT_SEARCH_CHILDREN);
	}

	// �������������� �������� �����������.
	_context = avcodec_alloc_context3(_encoder);
	if (_context == nullptr)
		return EncoderError::AllocContext;

	_context->bit_rate = bitRate;
	_context->width = width;
	_context->height = height;
	_context->time_base.num = 1;
	_context->time_base.den = fps; // ������ � �������.
	_context->gop_size = gopSize;
	_context->max_b_frames = 1;
	_context->pix_fmt = AV_PIX_FMT_YUV420P;

	int32_t success = avcodec_open2(_context, _encoder, nullptr);
	if (success != 0)
		return EncoderError::CodecOpen;

	// ������� ��������� ������ � ������.
	_frame = av_frame_alloc();
	_frame->format = _context->pix_fmt;
	_frame->width = _context->width;
	_frame->height = _context->height;

	int32_t allocated = av_image_alloc(_frame->data, _frame->linesize, _context->width, _context->height, _context->pix_fmt, 32);
	if (allocated < 0)
		return EncoderError::ImageAlloc;

	return EncoderError::NoErrors;
}

void Encoder::CloseEncoder()
{
	if (_context != nullptr)
	{
		avcodec_close(_context);
		av_freep(_context);
		_context = nullptr;
	}

	if (_encoder != nullptr)
	{
		av_freep(_encoder);
		_encoder = nullptr;
	}

	if (_frame != nullptr)
	{
		av_freep(&_frame->data[0]);
		av_frame_free(&_frame);
		_frame = nullptr;
	}
}

int32_t ReadStreamData(void* opaque, uint8_t buffer [], int32_t buf_size)
{
	Encoder* coder = static_cast<Encoder*>(opaque);
	coder->muxStreamCallback(buffer, buf_size);
	return 0;
}

EncoderError Encoder::CreateMuxStream(const char* containerName, int32_t bufferSize)
{
	// �������������� �������� ����������.
	_format = av_guess_format(containerName, nullptr, nullptr);
	if (_format == nullptr)
		return EncoderError::GuessFormat;

	_formatContext = avformat_alloc_context();
	if (_formatContext == nullptr)
		return EncoderError::AllocContext;

	const int32_t flags = AVFMT_GLOBALHEADER | AVFMT_TS_NEGATIVE | AVFMT_ALLOW_FLUSH;
	// ����������� ��������.
	_formatContext->oformat = _format;
	_formatContext->oformat->flags = flags;

	// ������� �����.
	_stream = avformat_new_stream(_formatContext, _encoder);
	if (_stream == nullptr)
		return EncoderError::NewStream;

	// ����������� �����.
	_stream->codecpar->width = _context->width;
	_stream->codecpar->height = _context->height;
	_stream->codecpar->bit_rate = _context->bit_rate;
	_stream->time_base.num = _context->time_base.num;
	_stream->time_base.den = _context->time_base.den;
	// _stream->codecpar->gop_size = _context->gop_size;
	// _stream->codecpar->max_b_frames = _context->max_b_frames;
	// _stream->codecpar->pix_fmt = _context->pix_fmt;

	// �������������� ����� ������.
	const int32_t writeableBuffer = 1;
	_streamBuffer = new uint8_t[bufferSize];
	_formatContext->pb = avio_alloc_context(_streamBuffer, bufferSize, 
		writeableBuffer, this, nullptr, ReadStreamData, nullptr);

	return EncoderError::NoErrors;
}

void Encoder::CloseStream()
{
	if (_streamBuffer != nullptr)
	{
		delete [] _streamBuffer;
		_streamBuffer = nullptr;
	}

	av_freep(_format);

	if (_formatContext)
	{
		av_freep(_formatContext->pb);
		avformat_free_context(_formatContext);
		_formatContext = nullptr;
	}
}

void WriteYuvFrame(uint8_t src[], AVFrame* dst, int32_t width, int32_t height)
{
	int32_t widthY = width;
	int32_t heightY = height;
	int32_t widthUV = widthY / 2;
	int32_t heightUV = heightY / 2;

	int32_t offsetU = widthY*heightY;
	int32_t offsetV = offsetU + widthUV*heightUV;

	uint8_t* srcY = src;
	uint8_t* srcU = src + offsetU;
	uint8_t* srcV = src + offsetV;

	uint8_t* dstY = dst->data[0];
	uint8_t* dstU = dst->data[1];
	uint8_t* dstV = dst->data[2];

	// Y-����������.
	for (int32_t y = 0; y < heightY; y++)
	{
		memcpy(dstY, srcY, widthY);
		dstY += dst->linesize[0];
		srcY += widthY;
	}

	// U,V-����������.
	for (int32_t y = 0; y < heightUV; y++)
	{
		memcpy(dstU, srcU, widthUV);
		srcU += y*widthUV;
		dstU += dst->linesize[1];

		memcpy(dstV, srcV, widthUV);
		srcV += widthUV;
		dstV += dst->linesize[2];
	}
}

int32_t Encoder::Encode(uint8_t data[], int64_t timestamp, int32_t& keyFrame) const
{
	// �������� ������
	WriteYuvFrame(data, _frame, _context->width, _context->height);

	// ������������� ����� �������.
	_frame->pts = timestamp;

	// �������������� �����.
	AVPacket* pkt = av_packet_alloc();
	pkt->data = nullptr;
	pkt->size = 0;

	// ��������.
	int32_t sendErrorCode = avcodec_send_frame(_context, _frame);
	if (sendErrorCode != 0)
		return sendErrorCode;

	int32_t recvErrorCode = avcodec_receive_packet(_context, pkt);
	if (recvErrorCode != 0)
		return recvErrorCode;

	keyFrame = pkt->flags;

	// ������������� ������� ���������, ���� ����� ��������� (AV_PKT_FLAG_CORRUPT).
	if (keyFrame > AV_PKT_FLAG_KEY)
		return 0;

	//int32_t size = pkt.size; // TODO return
	pkt->stream_index = _stream->index;
	int32_t result = av_write_frame(_formatContext, pkt);
	av_packet_free(&pkt);
	return result;
}

int32_t Encoder::WriteHeader() const
{
	int32_t result = avformat_write_header(_formatContext, nullptr);
	return result;
}

int32_t Encoder::WriteTrailer() const
{
	int32_t result = av_write_trailer(_formatContext);
	return result;
}
