﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Common.Annotations;
using Core;
using Core.Services;
using ServerLogic.VideoPlaying;

namespace ServerLogic
{
    internal class VideoPlayersFactory : IVideoPlayersFactory
    {
        [NotNull] private IVideoMuxersFactory VideoMuxersFactory { get; }

        public VideoPlayersFactory([NotNull] IVideoMuxersFactory videoMuxersFactory)
        {
            VideoMuxersFactory = videoMuxersFactory ?? throw new ArgumentNullException(nameof(videoMuxersFactory));
        }

        public async Task<IVideoPlayer> CreateVideoPlayerAsync(IRawVideoSource videoSource, IChannel channel, CancellationToken cancellationToken)
        {
            var muxer = await VideoMuxersFactory.CreateAsync(cancellationToken).ConfigureAwait(false);

            try
            {
                var player = new SimpleVideoPlayer(muxer, videoSource, channel);

                try
                {
                    await player.InitializeAsync(cancellationToken).ConfigureAwait(false);
                }
                catch (Exception)
                {
                    player.Dispose();
                    throw;
                }

                return player;
            }
            catch (Exception)
            {
                muxer.Dispose();
                throw;
            }
        }
    }
}
