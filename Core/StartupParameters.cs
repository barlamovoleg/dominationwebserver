﻿namespace Core
{
    public class StartupParameters
    {
        public string PluginsFolderPath { get; set; }

        public string RootAssembliesFolderPath { get; set; }
    }
}
