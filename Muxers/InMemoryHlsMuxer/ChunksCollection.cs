﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Muxers.InMemoryHlsMuxer
{
    internal class ChunksCollection : IDisposable
    {
        public int Capacity { get; }

        private readonly object _locker = new object();
        private readonly List<MpegTsChunk> _chunks = new List<MpegTsChunk>();

        public ChunksCollection(int capacity)
        {
            Capacity = capacity;
        }

        public void Dispose()
        {
            lock (_locker)
            {
                foreach (var mpegTsChunk in _chunks)
                {
                    mpegTsChunk.Dispose();
                }
                _chunks.Clear();
            }
        }

        public MpegTsChunk GetLastChunk()
        {
            lock (_locker)
            {
                return _chunks.LastOrDefault();
            }
        }

        public void AddChunk(MpegTsChunk chunk, int index)
        {
            lock (_locker)
            {
                _chunks.Add(chunk);

                if (_chunks.Count > Capacity)
                {
                    RemoveFirst();
                }
            }
        }

        private void RemoveFirst()
        {
            var first = _chunks.First();
            _chunks.Remove(first);
            first.Dispose();
        }
    }
}
