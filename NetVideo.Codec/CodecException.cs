﻿using System;

namespace NetVideo.Codec
{
    public abstract class CodecException : Exception
    {
        public int ErrorCode { get; }

        public CodecException(int errorCode)
        {
            ErrorCode = errorCode;
        }

        public CodecException(int errorCode, string message)
            : base(message)
        {
            ErrorCode = errorCode;
        }

        public CodecException(int errorCode, string message, Exception inner)
            : base(message, inner)
        {
            ErrorCode = errorCode;
        }
    }
}
