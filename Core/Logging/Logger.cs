﻿using Microsoft.Extensions.Logging;
using System;
using Common.Annotations;

namespace Core.Logging
{
    public static class Logger
    {
        private static ILogger _logger;
        private static readonly EventId EvId = new EventId();

        public static void Initialize([NotNull] ILogger loggerInstance)
        {
            _logger = loggerInstance ?? throw new ArgumentNullException(nameof(loggerInstance));
        }

        public static void Log(string message, LogLevel logLevel = LogLevel.Information)
        {
            _logger.Log<object>(logLevel, EvId, EvId, null, (o, exception) => message);
        }

        public static void Log(Exception ex, LogLevel logLevel = LogLevel.Critical)
        {
            _logger.Log<object>(logLevel, EvId, EvId, null, (o, exception) => ex.ToString());
        }

        public static void Log(Exception ex, string message, LogLevel logLevel = LogLevel.Critical)
        {
            _logger.Log<object>(logLevel, EvId, EvId, null, (o, exception) => message + Environment.NewLine + ex.ToString());
        }
    }
}
