﻿using System.Threading;
using System.Threading.Tasks;

namespace Core.Services
{
    public interface IVideoMuxersFactory
    {
        Task<IVideoMuxer> CreateAsync(CancellationToken cancellationToken);
    }
}
