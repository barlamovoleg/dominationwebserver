﻿import { Component } from '@angular/core';
import { GridBase } from "../gridBase";

@Component({
    selector: 'app-grid6',
    templateUrl: './grid6.component.html',
    styleUrls: ['./grid6.component.scss']
})

export class Grid6Component extends GridBase {

    constructor() {
        super();
    }
}