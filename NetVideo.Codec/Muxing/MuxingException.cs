﻿using System;
using NetVideo.Codec.Interop;

namespace NetVideo.Codec.Muxing
{
    public class MuxingException : CodecException
    {
        public MuxingException(MuxerError muxerError)
            : base((int)muxerError)
        {
        }

        public MuxingException(MuxerError muxerError, string message) : base((int)muxerError, message)
        {
        }

        public MuxingException(MuxerError muxerError, string message, Exception inner) : base((int)muxerError, message, inner)
        {
        }
    }
}
