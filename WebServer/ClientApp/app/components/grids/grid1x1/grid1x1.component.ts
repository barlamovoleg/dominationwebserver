﻿import { Component, Input } from '@angular/core';
import { GridCell } from "../../../models/gridTypes";
import { GridBase } from "../gridBase";

@Component({
    selector: 'app-grid1x1',
    templateUrl: './grid1x1.component.html',
    styleUrls: ['./grid1x1.component.scss']
})
export class Grid1x1Component extends GridBase{

    constructor() {
        super();
    }
}