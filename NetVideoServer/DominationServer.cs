﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Annotations;
using Core;
using Core.Logging;
using NetVideo;

namespace NetVideoServer
{
    internal class DominationServer : IServer
    {
        public event EventHandler Disconnected;
        public event EventHandler ConnectionLost;
        public event EventHandler ConnectionRestored;

        public ReadOnlyObservableCollection<IChannel> Channels { get; }

        public bool IsConnected { get; private set; }

        private NetVideo.NetVideoServer NetVideoServer { get; }

        private readonly TimeSpan _reconnectPeriod = TimeSpan.FromSeconds(10);
        private readonly CancellationTokenSource _lifeTimeToken = new CancellationTokenSource();
        private readonly ObservableCollection<IChannel> _channels = new ObservableCollection<IChannel>();

        public DominationServer([NotNull] NetVideo.NetVideoServer netVideoServer)
        {
            NetVideoServer = netVideoServer ?? throw new ArgumentNullException(nameof(netVideoServer));

            Channels = new ReadOnlyObservableCollection<IChannel>(_channels);
            OnServerReconnected();

            NetVideoServer.Disconnected += NetVideoServerOnDisconnected;
        }

        public override string ToString()
        {
            return $"{NetVideoServer.Host}:{NetVideoServer.Port}";
        }

        public void Disconnect()
        {
            NetVideoServer.Disconnected -= NetVideoServerOnDisconnected;

            IsConnected = false;
            NetVideoServer.Close();

            Disconnected?.Invoke(this, EventArgs.Empty);

            DestroyChannels();
        }

        public void Dispose()
        {
            _lifeTimeToken.Cancel();
            _lifeTimeToken.Dispose();

            if (IsConnected)
                Disconnect();
        }

        private void OnServerReconnected()
        {
            _channels.Clear();

            var channels = NetVideoServer.Channels.Select(CreateChannel);
            var altChannels = NetVideoServer.Channels.Select(CreateAlternativeChannel);
            foreach (var dominationChannel in channels)
            {
                _channels.Add(dominationChannel);
            }
            foreach (var altChannel in altChannels)
            {
                _channels.Add(altChannel);
            }
        }

        private IChannel CreateChannel(NetVideoChannel rawChannel)
        {
            return new DominationChannel(this, rawChannel);
        }

        private IChannel CreateAlternativeChannel(NetVideoChannel rawChannel)
        {
            return new DominationChannel(this, rawChannel, true);
        }

        private async Task ReconnectAsync(CancellationToken cancellationToken)
        {
            while (true)
            {
                try
                {
                    await NetVideoServer.ConnectAsync(cancellationToken).ConfigureAwait(false);

                    try
                    {
                        OnServerReconnected();
                        ConnectionRestored?.Invoke(this, EventArgs.Empty);
                    }
                    catch (Exception)
                    {
                        //ignore
                    }

                    return;
                }
                catch (Exception e)
                {
                    Logger.Log(e, $"Restore connection {this} failed");

                    await Task.Delay(_reconnectPeriod, cancellationToken).ConfigureAwait(false);
                }
            }
        }

        private void NetVideoServerOnDisconnected(object sender, DisconnectedEventArgs e)
        {
            try
            {
                ConnectionLost?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception)
            {
                //ignore
            }

            DestroyChannels();

            ReconnectAsync(_lifeTimeToken.Token).GetAwaiter();
        }

        private void DestroyChannels()
        {
            foreach (var channel in Channels)
            {
                channel.Dispose();
            }

            _channels.Clear();
        }
    }
}
