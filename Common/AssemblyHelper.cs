﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Common
{
    public static class AssemblyHelper
    {
        public static IEnumerable<Type> GetAllImplementingInterface<T>()
        {
            return GetAllImplementingInterface(typeof(T));
        }

        public static IEnumerable<Type> GetAllImplementingInterface(Type interfaceType)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            return assemblies
                .SelectMany(GetAssemblyTypes)
                .Where(t => !t.IsInterface)
                .Where(t => !t.IsAbstract)
                .Where(interfaceType.IsAssignableFrom);
        }

        private static IReadOnlyList<Type> GetAssemblyTypes(Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Array.Empty<Type>();
            }
        }
    }
}
