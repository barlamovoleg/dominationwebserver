﻿import { Input } from '@angular/core';
import { GridCell } from "../../models/gridTypes";
import {SwapCellsEventArgs} from "../grid-cell/grid-cell.component"

export class GridBase {
    @Input() public cells: GridCell[] | undefined;

    private onCellsSwap(eventArgs: SwapCellsEventArgs) {
        var cellsArray = this.cells as GridCell[];
        if (cellsArray == undefined)
            return;

        var cellsCount = cellsArray.length;

        //consider that all args is json objects copy
        var targetIndex = eventArgs.targetGridCell.index;
        var sourceIndex = eventArgs.sourceGridCell.index;

        if (targetIndex < 0 ||
            targetIndex >= cellsCount ||
            sourceIndex < 0 ||
            sourceIndex >= cellsCount ||
            targetIndex === sourceIndex)
            return;

        var targetGridCell = this.findCellByIndex(targetIndex);
        var sourceGridCell = this.findCellByIndex(sourceIndex);
        if (targetGridCell == null || sourceGridCell == null)
            return;

        sourceGridCell.index = targetIndex;
        targetGridCell.index = sourceIndex;
    }

    public findCellByIndex(index: number): GridCell | null {
        var cellsArray = this.cells as GridCell[];
        if (cellsArray == undefined)
            return null;

        for (var cell of cellsArray) {
            if (cell.index === index)
                return cell;
        }

        return null;
    }
}