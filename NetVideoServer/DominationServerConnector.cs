﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.Config;
using Core.Logging;

namespace NetVideoServer
{
    internal class DominationServerConnector : IServerConnector
    {
        private const ushort DefaultPort = 7000;

        public string TargetServerType { get; } = nameof(DominationServer);

        public async Task<IServer> ConnectToServerAsync(ServerInfo serverInfo, CancellationToken cancellationToken)
        {
            var host = serverInfo.ServerHost;
            var port = (ushort) (serverInfo.ServerPort ?? DefaultPort);

            var rawServer = new NetVideo.NetVideoServer(host, port);
            var authInfo = serverInfo.AuthInfo;

            try
            {
                rawServer.SetCredentials(authInfo.Login, authInfo.Password);
            }
            catch (Exception e)
            {
                Logger.Log(e, $"Invalid credentials {host}:{port}");
                throw;
            }

            try
            {
                await rawServer.ConnectAsync(cancellationToken).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logger.Log(e, $"Error while connect to server {host}:{port}");

                rawServer.Close();
                throw;
            }

            return new DominationServer(rawServer);
        }
    }
}
