﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using Common.Annotations;
using Core;
using NetVideo;

namespace NetVideoServer
{
    internal class DominationChannel : IChannel
    {
        public IServer ServerOwner { get; }

        public bool IsHidden => AlternativeStream;

        public int ChannelID { get; private set; }
        public string Name { get; private set; }
        public string Hierarchy { get; private set; }

        public NetVideoChannel NetVideoChannel { get; }
        public bool AlternativeStream { get; }

        public DominationChannel([NotNull] IServer serverOwner, [NotNull] NetVideoChannel netVideoChannel, bool alternativeStream = false)
        {
            ServerOwner = serverOwner ?? throw new ArgumentNullException(nameof(serverOwner));
            NetVideoChannel = netVideoChannel ?? throw new ArgumentNullException(nameof(netVideoChannel));
            AlternativeStream = alternativeStream;
            Name = NetVideoChannel.Name;
            ChannelID = NetVideoChannel.ChannelID;
            Hierarchy = GetHierarchy(NetVideoChannel.Location);
            
            NetVideoChannel.PropertyChanged += NetVideoChannelOnPropertyChanged;
        }

        public override string ToString()
        {
            return GetMarker();
        }

        public async Task<IRawVideoSource> GetVideoSourceAsync(CancellationToken cancellationToken)
        {
            var subscription = NetVideoChannel.Subscribe();
            subscription.UseSubstream = AlternativeStream;

            await subscription.ConnectAsync(cancellationToken).ConfigureAwait(false);

            return new VideoSource(subscription);
        }

        public string GetMarker()
        {
            var marker = $"{NetVideoChannel.Server.Host}/channel_{ChannelID}";
            if (AlternativeStream)
                marker += "$alt";

            return marker;
        }

        private string GetHierarchy(string location)
        {
            if (string.IsNullOrWhiteSpace(location))
            {
                if (!string.IsNullOrWhiteSpace(NetVideoChannel.Server.Host))
                    return $"{NetVideoChannel.Server.Host}:{NetVideoChannel.Server.Port}";

                return "unknown";
            }

            location = location.Trim('/', ' ');

            return location;
        }

        private void NetVideoChannelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(NetVideoChannel.ChannelID))
            {
                ChannelID = NetVideoChannel.ChannelID;
            }
            else if (e.PropertyName == nameof(NetVideoChannel.Name))
            {
                Name = NetVideoChannel.Name;
            }
            else if (e.PropertyName == nameof(NetVideoChannel.Location))
            {
                Hierarchy = GetHierarchy(NetVideoChannel.Location);
            }
        }

        public void Dispose()
        {
            NetVideoChannel.PropertyChanged -= NetVideoChannelOnPropertyChanged;
        }
    }
}