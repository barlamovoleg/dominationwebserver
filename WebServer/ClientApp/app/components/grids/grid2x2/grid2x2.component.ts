﻿import { Component } from '@angular/core';
import { GridBase } from "../gridBase";

@Component({
    selector: 'app-grid2x2',
    templateUrl: './grid2x2.component.html',
    styleUrls: ['./grid2x2.component.scss']
})
export class Grid2x2Component extends GridBase {

    constructor() {
        super();
    }
}