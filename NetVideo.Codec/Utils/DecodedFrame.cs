﻿using System;

namespace NetVideo.Codec
{
    /// <summary> Represents a decoded frame. </summary>
    public class DecodedFrame
    {
        public PixelFormat PixelFormat { get; set; }

        /// <summary> Frame width. </summary>
        public int Width { get; set; }

        /// <summary> Frame height. </summary>
        public int Height { get; set; }

        /// <summary> Frame stride0. </summary>
        public int Stride0 { get; set; }

        /// <summary> Frame stride1. </summary>
        public int Stride1 { get; set; }

        /// <summary> Frame stride2. </summary>
        public int Stride2 { get; set; }

        /// <summary> Frame plane0. </summary>
        public IntPtr Data0 { get; set; }

        /// <summary> Frame plane1. </summary>
        public IntPtr Data1 { get; set; }

        /// <summary> Frame plane2. </summary>
        public IntPtr Data2 { get; set; }

        public bool IsInInvertedColors { get; set; }
    }
}
