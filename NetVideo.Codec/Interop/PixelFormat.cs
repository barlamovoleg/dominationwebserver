﻿namespace NetVideo.Codec
{
    // ReSharper disable InconsistentNaming
    /// <summary> Represents pixel format. </summary>
    public enum PixelFormat
    {
        /// <summary> Unknown pixel format. </summary>
        None = -1,

        /// <summary>
        /// YUV420P pixel format.
        /// planar YUV 4:2:0, 12bpp,
        /// (1 Cr & Cb sample per 2x2 Y samples)
        /// </summary>
        YUV420P = 0,

        /// <summary>
        /// YUVJ420P pixel format.
        /// planar YUV 4:2:0, 12bpp, full scale (JPEG),
        /// deprecated in favor of AV_PIX_FMT_YUV420P and setting color_range
        /// </summary>
        YUVJ420P = 12,

        /// <summary> YUV422 pixel format. </summary>
        YUYV422 = 1,

        /// <summary> B8G8R8A8 pixel format. </summary>
        BGRA = 28,

        /// <summary> A8R8G8B8 pixel format. </summary>
        ARGB = 25,

        /// <summary>
        /// R8G8B8 pixel format.
        /// packed RGB 8:8:8, 24bpp, RGBRGB...
        /// </summary>
        RGB24 = 2,

        /// <summary> 
        /// B8G8A8 pixel format.
        /// packed RGB 8:8:8, 24bpp, BGRBGR...
        /// </summary>
        BGR24 = 3,

        /// <summary>
        /// planar YUV 4:2:0, 12bpp,
        /// 1 plane for Y and 1 plane for the UV components,
        /// which are interleaved (first byte U and the following byte V)
        /// </summary>
        NV12 = 23,

        /// <summary>
        /// planar YUV 4:2:0, 12bpp,
        /// 1 plane for Y and 1 plane for the UV components,
        /// which are interleaved (first byte V and the following byte U)
        /// </summary>
        NV21 = 24
    }
    // ReSharper restore InconsistentNaming
}