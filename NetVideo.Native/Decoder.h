#pragma once

enum class DecoderError
{
    NoErrors = 0,
    FindDecoder = 1,
    AllocContext = 2,
    CodecOpen = 3
};

// return 0 on success, negative on error
using DecodeCB = int32_t(*)(void* state);
