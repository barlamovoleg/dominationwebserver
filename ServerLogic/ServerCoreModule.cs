﻿using Common.Annotations;
using Core;
using Core.Services;
using Microsoft.Extensions.DependencyInjection;
using ServerLogic.Config;

namespace ServerLogic
{
    [UsedImplicitly]
    public class ServerCoreModule : IServicesModule
    {
        public void RegisterServices(IServiceCollection serviceCollection)
        {
            var serverController = new ServersController();

            serviceCollection.AddSingleton<IConfigProvider, JsonFileConfigProvider>();
            serviceCollection.AddSingleton<IChannelsProvider, ChannelsProvider>();
            serviceCollection.AddSingleton<IServersProvider>(serverController);
            serviceCollection.AddSingleton(serverController);

            serviceCollection.AddSingleton<IVideoPlayersFactory, VideoPlayersFactory>();
            serviceCollection.AddSingleton<IVideoPlayersService, VideoPlayerService>();
            serviceCollection.AddSingleton<IVideoPlayersLifeTimesService, VideoPlayersLifeTimesService>();

            serviceCollection.AddSingleton<AllServersConnector>();
            
        }
    }
}
