﻿using System;
using Core;

namespace Muxers
{
    public interface IIndexedMuxer : IVideoMuxer
    {
        Guid Id { get; }
    }
}
