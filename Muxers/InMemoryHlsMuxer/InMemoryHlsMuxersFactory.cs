﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Common.Annotations;
using Core;
using Core.Services;

namespace Muxers.InMemoryHlsMuxer
{
    internal class InMemoryHlsMuxersFactory : IVideoMuxersFactory
    {
        private MuxersCollection MuxersCollection { get; }

        public InMemoryHlsMuxersFactory([NotNull] MuxersCollection muxersCollection)
        {
            MuxersCollection = muxersCollection ?? throw new ArgumentNullException(nameof(muxersCollection));
        }

        public Task<IVideoMuxer> CreateAsync(CancellationToken cancellationToken)
        {
            var muxer = new InMemoryHlsMuxer(2, TimeSpan.FromSeconds(5));
            MuxersCollection.AddMuxer(muxer);

            return Task.FromResult((IVideoMuxer) muxer);
        }
    }
}
