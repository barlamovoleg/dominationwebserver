#include "stdafx.h"
#include "DecoderContext.h"

DecoderContext::DecoderContext() :
	_context(nullptr), _decodedFrame(nullptr)
{
}

DecoderContext::~DecoderContext()
{
	avcodec_free_context(&_context);
	av_frame_free(&_decodedFrame);
	//av_freep(_decoder);
}

DecoderError DecoderContext::OpenDecoderContext(AVCodec* codec, AVCodecParameters* parameters)
{
	if (codec == nullptr)
		return DecoderError::FindDecoder;

	_context = avcodec_alloc_context3(codec);
	if (_context == nullptr)
		return DecoderError::AllocContext;

    if (parameters != nullptr)
    {
        if (avcodec_parameters_to_context(_context, parameters) < 0)
            return DecoderError::AllocContext;
    }

    if ((codec->capabilities & AV_CODEC_CAP_TRUNCATED) != 0)
        _context->flags |= AV_CODEC_FLAG_TRUNCATED;

	const int32_t result = avcodec_open2(_context, codec, nullptr);
	if (result < 0)
		return DecoderError::CodecOpen;

	_decodedFrame = av_frame_alloc();

	return DecoderError::NoErrors;
}

int32_t DecoderContext::Push(uint8_t data[], const int32_t size) const
{
	AVPacket packet;
	av_init_packet(&packet);
	packet.data = data;
	packet.size = size;

	return Push(&packet);
}

int32_t DecoderContext::Push(const AVPacket* packet) const
{
    const int32_t sendErrorCode = avcodec_send_packet(_context, packet);
    return sendErrorCode;
}

int32_t DecoderContext::Pop() const
{
	const int32_t recvErrorCode = avcodec_receive_frame(_context, _decodedFrame);
	return recvErrorCode;
}

int32_t DecoderContext::Decode(uint8_t data[], const int32_t size, const DecodeCB callback, void* state) const
{
	AVPacket packet;
	av_init_packet(&packet);
	packet.data = data;
	packet.size = size;

	return Decode(&packet, callback, state);
}

int32_t DecoderContext::Decode(const AVPacket* packet, const DecodeCB callback, void* state) const
{
    int32_t ret = avcodec_send_packet(_context, packet);
    // Again EAGAIN is not expected
    if (ret >= 0)
    {
        while (!ret)
        {
            ret = avcodec_receive_frame(_context, _decodedFrame);
            if (!ret)
                ret = callback(state);
        }
    }

    if (ret == AVERROR(EAGAIN))
        return 0;

    return ret;
}

int32_t DecoderContext::Decode(uint8_t data[], const int32_t size) const
{
	AVPacket packet;
	av_init_packet(&packet);
	packet.data = data;
	packet.size = size;

	return Decode(&packet);
}

int32_t DecoderContext::Decode(const AVPacket* packet) const
{
	const int32_t sendErrorCode = avcodec_send_packet(_context, packet);
	if (sendErrorCode < 0)
		return +abs(sendErrorCode);

	const int32_t recvErrorCode = avcodec_receive_frame(_context, _decodedFrame);
	if (recvErrorCode < 0)
		return -abs(recvErrorCode);

	return 0;
}

AVFrame* DecoderContext::GetCurrentFrame() const
{
	return _decodedFrame;
}
