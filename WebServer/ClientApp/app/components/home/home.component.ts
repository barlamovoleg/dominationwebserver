import { Component, ViewChild } from '@angular/core';
import { ChannelsProvider } from "../../services/channels-provider-service"
import {ChannelsTreeComponent} from "../channels-tree/channels-tree.component"

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent {

    @ViewChild(ChannelsTreeComponent) channelsTree: ChannelsTreeComponent;

    isSettingsWindowShowed: boolean = false;
    isGridEditMode: boolean = false;

    constructor(private channelsProvider: ChannelsProvider) {

    }

    ngOnInit() {
        this.channelsProvider.preLoadIfNeed();
    }

    onLeftPanelSettingsButtonClick() {
        this.isSettingsWindowShowed = !this.isSettingsWindowShowed;
    }

    onTopPanelSettingsButtonClick() {
        this.isGridEditMode = !this.isGridEditMode;
        this.channelsProvider.preLoadIfNeed();
        this.channelsTree.onOpened();
    }

    onCloseSettingsWindow() {
        this.isSettingsWindowShowed = false;
    }
}