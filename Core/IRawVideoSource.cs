﻿using System;
using System.Threading.Channels;
using Core.Messages;

namespace Core
{
    public interface IRawVideoSource : IDisposable
    {
        ChannelReader<VideoMessage> VideoStream { get; }
    }
}