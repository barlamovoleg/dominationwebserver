﻿using System;
using System.Runtime.Serialization;
using Common;

namespace Core.Config
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class ServerInfo : NotifyObject
    {
        [DataMember]
        public string TargetServerType { get; set; }
        [DataMember]
        public string ServerHost { get; set; }
        [DataMember]
        public uint? ServerPort { get; set; }
        [DataMember]
        public AuthInfo AuthInfo { get; set; }
    }
}
