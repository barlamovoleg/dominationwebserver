﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Annotations;
using Core;
using Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Muxers;
using WebServer.Models;

namespace WebServer.Controllers
{
    public class InternalApiController
    {
        private IChannelsProvider ChannelsProvider { get; }
        private IVideoPlayersService VideoPlayersService { get; }
        private MuxersCollection MuxersCollection { get; }

        public InternalApiController([NotNull] IChannelsProvider channelsProvider, [NotNull] IVideoPlayersService videoPlayersService, [NotNull] MuxersCollection muxersCollection)
        {
            ChannelsProvider = channelsProvider ?? throw new ArgumentNullException(nameof(channelsProvider));
            VideoPlayersService = videoPlayersService ?? throw new ArgumentNullException(nameof(videoPlayersService));
            MuxersCollection = muxersCollection ?? throw new ArgumentNullException(nameof(muxersCollection));
        }

        [PublicAPI]
        [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
        public IEnumerable<ChannelDto> Channels()
        {
            var channels = ChannelsProvider.GetChannels().Where(channel => !channel.IsHidden);
            var dtoChannels = GetChannelsDto(channels);
            return dtoChannels;
        }

        [PublicAPI]
        [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
        public async Task<IActionResult> Hls(string parameters)
        {
            var channel = FindChannelByMarker(parameters);
            if (channel == null)
                return new BadRequestResult();


            var player = await VideoPlayersService.GetOrCreatePlayerAsync(channel, CancellationToken.None);
            return new FileStreamResult(player.VideoStream, player.ContentType);

            //var fileContent = File.ReadAllBytes("1/file.m3u8");
            //return new FileStreamResult(new MemoryStream(fileContent), "application/x-mpegURL");
        }

        [PublicAPI]
        [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
        public IActionResult Chunk(string parameters)
        {
            var chunkMarker = parameters;

            var chunkStream = MuxersCollection.GetChunkData(chunkMarker);
            return new FileStreamResult(chunkStream, "application/octet-stream");

            //var bytes = File.ReadAllBytes("1/out_res");
            //return new FileStreamResult(new MemoryStream(bytes), "application/octet-stream");
        }

        private IEnumerable<ChannelDto> GetChannelsDto(IEnumerable<IChannel> channels)
        {
            return channels.Select(channel => new ChannelDto
            {
                Name = channel.Name,
                Location = channel.Hierarchy,
                Marker = channel.GetMarker()
            });
        }

        private IChannel FindChannelByMarker(string channelMarker)
        {
            var channel = ChannelsProvider.FindChannelByMarker(channelMarker);
            if (channel != null)
                return channel;

            var altIndex = channelMarker.IndexOf("$alt", StringComparison.Ordinal);
            if (altIndex < 0)
                return null;

            var l = channelMarker.Length;
            var markerWithoutAlt = channelMarker.Substring(0, l - (l - altIndex));
            if (string.IsNullOrWhiteSpace(markerWithoutAlt))
                return null;

            return ChannelsProvider.FindChannelByMarker(markerWithoutAlt);
        }
    }
}