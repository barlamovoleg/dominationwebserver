#include "stdafx.h"

//#define BUILD_DECODER_API
//#define BUILD_CONVERT_API
//#define BUILD_CISCO_DECODER_API
//#define BUILD_CISCO_ENCODER_API
//#define BUILD_ENCODER_API
//#define BUILD_EXTRACTOR_API
#define BUILD_MUXER_API

#define DllExport _declspec(dllexport)
#define CALL __cdecl

#ifdef BUILD_DECODER_API
extern "C"
{
#include "CodecType.h"
#include "DecoderContext.h"
#include "VideoDecoder.h"

    DllExport AVCodec* CALL FindDecoder(CodecType codecType)
    {
        return avcodec_find_decoder(ToAVCodecID(codecType));
    }

    DllExport AVCodec* CALL FindDecoderByAVCodecID(AVCodecID codecID)
    {
        return avcodec_find_decoder(codecID);
    }

    DllExport AVCodec* CALL FindDecoderByName(const char* decoderName)
    {
        return avcodec_find_decoder_by_name(decoderName);
    }

    DllExport DecoderContext* CALL CreateDecoder()
    {
        return (new DecoderContext());
    }

    DllExport void CALL DeleteDecoder(DecoderContext* context)
    {
        delete context;
    }

    DllExport DecoderError CALL OpenDecoderContext(DecoderContext* context, AVCodec* codec, AVCodecParameters* parameters)
    {
        return context->OpenDecoderContext(codec, parameters);
    }

    int32_t getBitsPerSample(CodecType codecType)
    {
        switch (codecType)
        {
			case CodecType::G726_16k: return 2;
			case CodecType::G726_24k: return 3;
			case CodecType::G726_32k: return 4;
			case CodecType::G726_40k: return 5;
			default:                  return 4;
        }
    }

    DllExport DecoderError OpenDecoder(DecoderContext* context, CodecType codecType, AVCodecParameters* parameters)
    {
        AVCodec* codec = FindDecoder(codecType);

        if (parameters == nullptr)
        {
            if (codecType == CodecType::MuLaw || codecType == CodecType::ALaw)
            {
                parameters = avcodec_parameters_alloc();
                //context
                
                parameters->sample_rate = 8000;
                parameters->bits_per_raw_sample = 8;

                parameters->format = AV_SAMPLE_FMT_S16;
                parameters->channels = 1;
                parameters->channel_layout = AV_CH_LAYOUT_MONO;                
                parameters->codec_id = codec->id;
                parameters->codec_type = codec->type;

                const auto error = context->OpenDecoderContext(codec, parameters);

                avcodec_parameters_free(&parameters);

                return error;
            }
            else if (codec->id == AV_CODEC_ID_ADPCM_G726)
            {
                parameters = avcodec_parameters_alloc();

                parameters->sample_rate = 8000;
                parameters->bits_per_coded_sample = getBitsPerSample(codecType);

				parameters->channels = 1;
                parameters->channel_layout = AV_CH_LAYOUT_MONO;
                parameters->format = AV_SAMPLE_FMT_S16;
                parameters->codec_id = codec->id;
                parameters->codec_type = codec->type;

                const auto error = context->OpenDecoderContext(codec, parameters);

                avcodec_parameters_free(&parameters);

                return error;
            }
        }

        return context->OpenDecoderContext(codec, parameters);
    }

    DllExport int32_t CALL Push(DecoderContext* context, uint8_t data[], const int32_t size)
    {
        return context->Push(data, size);
    }

    DllExport int32_t CALL PushPacket(DecoderContext* context, const AVPacket* packet)
    {
        return context->Push(packet);
    }

    DllExport int32_t CALL Pop(DecoderContext* context)
    {
        return context->Pop();
    }

    DllExport int32_t CALL DecodeWithCallback(DecoderContext* context, uint8_t data[], const int32_t size, DecodeCB callback, void* state)
    {
        return context->Decode(data, size, callback, state);
    }

    DllExport int32_t CALL DecodePacketWithCallback(DecoderContext* context, const AVPacket* packet, DecodeCB callback, void* state)
    {
        return context->Decode(packet, callback, state);
    }

    DllExport int32_t CALL DecodeBytes(DecoderContext* context, uint8_t data[], const int32_t size)
    {
        return context->Decode(data, size);
    }

    DllExport int32_t CALL DecodePacket(DecoderContext* context, AVPacket* packet)
    {
        return context->Decode(packet);
    }

    DllExport AVFrame* CALL GetCurrentFrame(DecoderContext* context)
    {
        return context->GetCurrentFrame();
    }

    DllExport int32_t CALL GetCurrentFrameFormat(DecoderContext* context)
    {
        return context->GetCurrentFrame()->format;
    }





    DllExport VideoDecoder* CALL VideoDecoder_Create()
    {
        return (new VideoDecoder());
    }

    DllExport void CALL VideoDecoder_Delete(VideoDecoder* decoder)
    {
        delete decoder;
    }

    DllExport DecoderError VideoDecoder_Open(VideoDecoder* decoder, CodecType codecType, AVCodecParameters* parameters)
    {
        AVCodec* codec = FindDecoder(codecType);

        return decoder->OpenDecoderContext(codec, parameters);
    }

    DllExport int32_t CALL VideoDecoder_Push(VideoDecoder* decoder, uint8_t data[], const int32_t size)
    {
        return decoder->Push(data, size);
    }

    DllExport int32_t CALL VideoDecoder_PushPacket(VideoDecoder* decoder, const AVPacket* packet)
    {
        return decoder->Push(packet);
    }

    DllExport int32_t CALL VideoDecoder_Pop(VideoDecoder* decoder, AVFrame* frame)
    {
        return decoder->Pop(frame);
    }

    DllExport AVFrame* CALL AVFrame_Create()
    {
        return av_frame_alloc();
    }

    DllExport void CALL AVFrame_Delete(AVFrame* frame)
    {
        av_frame_free(&frame);
    }
}
#endif // BUILD_DECODER_API

#ifdef BUILD_CONVERT_API
extern "C"
{
#include "ColorConvert.h"

    DllExport int32_t CALL ConvertFormats(ConversionParams* params)
    {
        return ColorConvert::Convert(params);
    }
}
#endif

#ifdef BUILD_CISCO_DECODER_API
extern "C"
{
#include <svc/codec_api.h>

    DllExport long ISVCDecoder_Initialize(ISVCDecoder* decoder, const SDecodingParam* pParam)
    {
        return decoder->Initialize(pParam);
    }

    DllExport long ISVCDecoder_Uninitialize(ISVCDecoder* decoder)
    {
        return decoder->Uninitialize();
    }

    DllExport DECODING_STATE ISVCDecoder_DecodeFrame(
        ISVCDecoder* decoder,
        const unsigned char* pSrc,
        const int iSrcLen,
        unsigned char** ppDst,
        int* pStride,
        int& iWidth,
        int& iHeight)
    {
        return decoder->DecodeFrame(pSrc, iSrcLen, ppDst, pStride, iWidth, iHeight);
    }

    DllExport DECODING_STATE ISVCDecoder_DecodeFrameNoDelay(ISVCDecoder* decoder,
        const unsigned char* pSrc,
        const int iSrcLen,
        unsigned char** ppDst,
        SBufferInfo* pDstInfo)
    {
        return decoder->DecodeFrameNoDelay(pSrc, iSrcLen, ppDst, pDstInfo);
    }

    DllExport DECODING_STATE ISVCDecoder_DecodeFrame2(ISVCDecoder* decoder,
        const unsigned char* pSrc,
        const int iSrcLen,
        unsigned char** ppDst,
        SBufferInfo* pDstInfo)
    {
        return decoder->DecodeFrame2(pSrc, iSrcLen, ppDst, pDstInfo);
    }

    DllExport DECODING_STATE ISVCDecoder_DecodeParser(ISVCDecoder* decoder,
        const unsigned char* pSrc,
        const int iSrcLen,
        SParserBsInfo* pDstInfo)
    {
        return decoder->DecodeParser(pSrc, iSrcLen, pDstInfo);
    }

    DllExport DECODING_STATE ISVCDecoder_DecodeFrameEx(ISVCDecoder* decoder,
        const unsigned char* pSrc,
        const int iSrcLen,
        unsigned char* pDst,
        int iDstStride,
        int& iDstLen,
        int& iWidth,
        int& iHeight,
        int& iColorFormat)
    {
        return decoder->DecodeFrameEx(pSrc, iSrcLen, pDst, iDstStride, iDstLen, iWidth, iHeight, iColorFormat);
    }

    DllExport long ISVCDecoder_SetOption(ISVCDecoder* decoder, DECODER_OPTION eOptionId, void* pOption)
    {
        return decoder->SetOption(eOptionId, pOption);
    }

    DllExport long ISVCDecoder_GetOption(ISVCDecoder* decoder, DECODER_OPTION eOptionId, void* pOption)
    {
        return decoder->GetOption(eOptionId, pOption);
    }
}
#endif BUILD_CISCO_DECODER_API

#ifdef BUILD_CISCO_ENCODER_API
extern "C"
{
#include <svc/codec_api.h>

    DllExport int ISVCEncoder_Initialize(ISVCEncoder* encoder, const SEncParamBase* pParam)
    {
        return encoder->Initialize(pParam);
    }

    DllExport int ISVCEncoder_InitializeEx(ISVCEncoder* encoder, const SEncParamExt* pParam)
    {
        return encoder->InitializeExt(pParam);
    }

    DllExport int ISVCEncoder_GetDefaultParams(ISVCEncoder* encoder, SEncParamExt* pParam)
    {
        return encoder->GetDefaultParams(pParam);
    }

    DllExport int ISVCEncoder_Uninitialize(ISVCEncoder* encoder)
    {
        return encoder->Uninitialize();
    }

    DllExport int ISVCEncoder_EncodeFrame(ISVCEncoder* encoder, const SSourcePicture* kpSrcPic, SFrameBSInfo* pBsInfo)
    {
        return encoder->EncodeFrame(kpSrcPic, pBsInfo);
    }

    DllExport int ISVCEncoder_EncodeParameterSets(ISVCEncoder* encoder, SFrameBSInfo* pBsInfo)
    {
        return encoder->EncodeParameterSets(pBsInfo);
    }

    DllExport int ISVCEncoder_ForceIntraFrame(ISVCEncoder* encoder, bool bIDR)
    {
        return encoder->ForceIntraFrame(bIDR);
    }

    DllExport int ISVCEncoder_SetOption(ISVCEncoder* encoder, ENCODER_OPTION eOptionId, void* pOption)
    {
        return encoder->SetOption(eOptionId, pOption);
    }

    DllExport int ISVCEncoder_GetOption(ISVCEncoder* encoder, ENCODER_OPTION eOptionId, void* pOption)
    {
        return encoder->GetOption(eOptionId, pOption);
    }
}
#endif BUILD_CISCO_ENCODER_API

#ifdef BUILD_ENCODER_API
extern "C"
{
#include "Encoder.h"
    DllExport Encoder* CALL CreateEncoder(CodecType codecType)
    {
        return (new Encoder(ToAVCodecID(codecType)));
    }

    DllExport EncoderError CALL OpenEncoder(Encoder* encoder, int32_t width, int32_t height, int32_t fps, int32_t bitRate, int32_t gopSize)
    {
        return encoder->OpenEncoder(width, height, fps, bitRate, gopSize);
    }

    DllExport int32_t CALL Encode(Encoder* encoder, uint8_t data[], int64_t timestamp, int32_t& keyFrame)
    {
        return encoder->Encode(data, timestamp, keyFrame);
    }

    DllExport EncoderError CALL CreateMuxStream(Encoder* encoder, char* containerName, int32_t bufferSize)
    {
        return encoder->CreateMuxStream(containerName, bufferSize);
    }

    DllExport void CALL DeleteEncoder(Encoder* encoder)
    {
        delete encoder;
    }

    DllExport void CALL SetStreamCallback(Encoder* encoder, MuxStreamCB callback)
    {
        encoder->muxStreamCallback = callback;
    }

    DllExport int32_t CALL WriteHeader(Encoder* encoder)
    {
        return encoder->WriteHeader();
    }
    DllExport int32_t CALL WriteTrailer(Encoder* encoder)
    {
        return encoder->WriteTrailer();
    }
}
#endif BUILD_ENCODER_API

#ifdef BUILD_EXTRACTOR_API
extern "C"
{
#include "MediaExtractor.h"
    DllExport MediaExtractor* CALL CreateMediaExtractor()
    {
        return (new MediaExtractor());
    }

    DllExport int32_t CALL InitializeMediaExtractor(MediaExtractor* extractor, const char* filename)
    {
        return extractor->Initialize(filename);
    }

    DllExport void CALL DumpFileInformation(MediaExtractor* extractor, const char* filename)
    {
        extractor->DumpInformation(filename);
    }

    DllExport AVCodecParameters* CALL GetCodecParameters(MediaExtractor* extractor, int32_t streamIndex)
    {
        return extractor->GetCodecParameters(streamIndex);
    }

    DllExport int32_t CALL FindVideoStream(MediaExtractor* extractor, CodecType& codecType)
    {
        AVCodecID codecID;
        const int32_t idx = extractor->FindStream(AVMEDIA_TYPE_VIDEO, codecID);
        codecType = ToCodecType(codecID);
        return idx;
    }

    DllExport int32_t CALL FindAudioStream(MediaExtractor* extractor, CodecType& codecType)
    {
        AVCodecID codecID;
        const int32_t idx = extractor->FindStream(AVMEDIA_TYPE_AUDIO, codecID);
        codecType = ToCodecType(codecID);
        return idx;
    }

    DllExport int32_t CALL FindSubtitleStream(MediaExtractor* extractor, CodecType& codecType)
    {
        AVCodecID codecID;
        const int32_t idx = extractor->FindStream(AVMEDIA_TYPE_SUBTITLE, codecID);
        codecType = ToCodecType(codecID);
        return idx;
    }

    DllExport bool CALL ReadFrame(MediaExtractor* extractor, AVPacket* packet)
    {
        return extractor->ReadFrame(packet);
    }

    DllExport AVPacket* CALL CreatePacket()
    {
        return av_packet_alloc();
    }

    DllExport bool CALL Seek(MediaExtractor* extractor, int streamIndex, double seekSec)
    {
        return extractor->Seek(streamIndex, seekSec);
    }
    DllExport double CALL GetFps(MediaExtractor* extractor, int streamIndex)
    {
        return extractor->GetFps(streamIndex);
    }
    DllExport int64_t CALL GetFrameCount(MediaExtractor* extractor, int streamIndex)
    {
        return extractor->GetFrameCount(streamIndex);
    }
    DllExport void CALL DisposePacket(AVPacket* packet)
    {
        // Free the packet that was allocated by av_read_frame
        av_packet_free(&packet);
    }

    DllExport void CALL DeleteMediaExtractor(MediaExtractor* extractor)
    {
        delete extractor;
    }
}
#endif BUILD_EXTRACTOR_API

#ifdef BUILD_MUXER_API

extern "C"
{
#include "MuxerContext.h"
#include "MuxerType.h"
#include "MuxerError.h"

	DllExport MuxerContext* CALL CreateMuxerContext()
	{
		return (new MuxerContext());
	}

	DllExport AVOutputFormat* CALL FindMuxer(MuxerType muxerType)
	{
		const char* muxerName = ToMuxerName(muxerType);
		return av_guess_format(muxerName, NULL, NULL);
	}

	DllExport AVOutputFormat* CALL FindMuxerByName(const char* muxerName)
	{
		return av_guess_format(muxerName, NULL, NULL);
	}

	DllExport MuxerError CALL OpenMuxer(MuxerContext* context, MuxerType muxerType)
	{
		AVOutputFormat* muxer = FindMuxer(muxerType);
		return context->Open(muxer);
	}

	//timestamp - in seconds from first frame of stream
	DllExport MuxerError CALL Push(MuxerContext* context, uint8_t data[], const int32_t size, const long timestamp, bool isKeyFrame)
	{
		return context->Push(data, size, timestamp, isKeyFrame);
	}

	DllExport MuxerError CALL Close(MuxerContext* context, uint8_t outputData[])
	{
		return context->Close(outputData);
	}

	DllExport int CALL GetLength(MuxerContext* context)
	{
		return context->GetLength();
	}

	DllExport void CALL DeleteMuxerContext(MuxerContext* context)
	{
		delete context;
	}
}

#endif BUILD_MUXER_API
