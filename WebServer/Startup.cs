using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Timers;
using Core;
using Core.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ServerLogic;
using Timer = System.Timers.Timer;

namespace WebServer
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public ILoggerFactory LoggerFactory { get; }

        private readonly ServerLogicRoot _server;
        private readonly CancellationTokenSource _lifeTimeToken = new CancellationTokenSource();

        private readonly Timer _systemLogTimer = new Timer();

        public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            Configuration = configuration;
            LoggerFactory = loggerFactory;

            var mainLogger = LoggerFactory.CreateLogger("main");
            Logger.Initialize(mainLogger);

            _systemLogTimer.Elapsed += SystemLogTimerOnElapsed;
            _systemLogTimer.Interval = 5 * 1000;
            _systemLogTimer.Start();

            var startupParameters = new StartupParameters
            {
                RootAssembliesFolderPath = GetRootPath(),
                PluginsFolderPath = string.Empty
            };

            _server = new ServerLogicRoot(startupParameters);

            Logger.Log("Server initializing...");
        }

        private void SystemLogTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            Logger.Log($"THREADS: {Process.GetCurrentProcess().Threads.Count}");
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Logger.Log("Configure services...");

            try
            {
                using (var preInitTokenSource = new CancellationTokenSource(Timeouts.LoadAppDefault))
                {
                    _server.PreInitAsync(preInitTokenSource.Token).Wait(preInitTokenSource.Token);
                }

                using (var registeringTokenSource = new CancellationTokenSource(Timeouts.LoadAppDefault))
                {
                    _server.RegisterServicesAsync(services, registeringTokenSource.Token).Wait(registeringTokenSource.Token);
                }

                services.AddMvc();
            }
            catch (OperationCanceledException)
            {
                Logger.Log("Timeout...");
                throw;
            }
            catch (Exception e)
            {
                Logger.Log(e);
                throw;
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime applicationLifetime)
        {
            Logger.Log("Configure server and routing...");

            try
            {
                using (var postLoadTokenSource = new CancellationTokenSource(Timeouts.LoadAppDefault))
                {
                    _server.PostInitAsync(app.ApplicationServices, postLoadTokenSource.Token).Wait(postLoadTokenSource.Token);
                }

                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                    app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                    {
                        HotModuleReplacement = true
                    });
                }
                else
                {
                    app.UseExceptionHandler("/Home/Error");
                }

                app.UseStaticFiles(new StaticFileOptions
                {
                    ServeUnknownFileTypes = true,
                    DefaultContentType = "text/plain"
                });

                app.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "internalApi",
                        template: "api/{action}/{*parameters}",
                        defaults: new {controller = "InternalApi"});

                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}");

                    routes.MapSpaFallbackRoute(
                        name: "spa-fallback",
                        defaults: new {controller = "Home", action = "Index"});
                });

                //Facking magic number three!
                AppContext.SetSwitch("Switch.Microsoft.AspNetCore.Mvc.EnableRangeProcessing", true);

                applicationLifetime.ApplicationStopping.Register(OnShutdown);

                //root call async
                Logger.Log("root server call async...");
                _server.StartAsync(_lifeTimeToken.Token);


            }
            catch (OperationCanceledException)
            {
                Logger.Log("Timeout...");
                throw;
            }
            catch (Exception e)
            {
                Logger.Log(e);
                throw;
            }
        }

        private void OnShutdown()
        {
            Logger.Log("app exiting...");

            _lifeTimeToken.Cancel();
            _lifeTimeToken.Dispose();

            _server.Dispose();

            _systemLogTimer.Stop();
            _systemLogTimer.Dispose();
        }

        private static string GetRootPath()
        {
            var currentAssembly = Assembly.GetAssembly(typeof(Startup));
            var currentAssemblyPath = currentAssembly.Location;
            return Path.GetDirectoryName(currentAssemblyPath);
        }
    }
}
