﻿using System;

namespace Core.Messages
{
    public class VideoMessage : ChannelMessage
    {
        /// <summary> Whether pictire colors are inverted, or not. </summary>
        public bool IsInvertedColors { get; set; }

        /// <summary> Whether pictire is key frame, or not. </summary>
        public bool IsKeyFrame { get; set; }

        /// <summary> Whether is substream picture, or not. </summary>
        public bool IsSubstream { get; set; }

        /// <summary> Whether is has alarm or not. </summary>
        public bool HasAlarm { get; set; }

        /// <summary> Decoder parameter pointer, if any. </summary>
        public IntPtr DecoderParameters { get; set; }

        public VideoMessage Clone()
        {
            return new VideoMessage
            {
                IsSubstream = IsSubstream,
                DecoderParameters = DecoderParameters,
                IsInvertedColors = IsInvertedColors,
                IsKeyFrame = IsKeyFrame,
                ChannelID = ChannelID,
                Data = Data,
                Timestamp = Timestamp,
                KeyFrameIdx = KeyFrameIdx,
                SequenceID = SequenceID,
                HasAlarm = HasAlarm,
            };
        }
    }
}
