﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Core.Config
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class WebServerConfig
    {
        [DataMember]
        public ObservableCollection<ServerInfo> ServersInfos { get; set; } = new ObservableCollection<ServerInfo>();
    }
}