﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Core.Config;
using Core.Logging;
using Core.Services;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace ServerLogic.Config
{
    internal class JsonFileConfigProvider : IConfigProvider
    {
        private readonly string _configFilePath;
        private const string ConfigFileName = "server.config";

        private WebServerConfig _bufferedConfig;

        public JsonFileConfigProvider()
        {
            var rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            _configFilePath = Path.Combine(rootPath, "config", ConfigFileName);
        }

        public async Task<WebServerConfig> GetConfigAsync(CancellationToken cancellationToken)
        {
            if (_bufferedConfig != null)
                return _bufferedConfig;

            if (!File.Exists(_configFilePath))
            {
                return await GenerateConfigAsync(cancellationToken).ConfigureAwait(false);
            }

            var loadedConfig = await TryLoadConfigAsync(cancellationToken).ConfigureAwait(false);

            return loadedConfig ?? await GenerateConfigAsync(cancellationToken).ConfigureAwait(false);
        }

        private async Task<WebServerConfig> GenerateConfigAsync(CancellationToken cancellationToken)
        {
            _bufferedConfig = new WebServerConfig();
            await SaveConfigAsync(_bufferedConfig, cancellationToken).ConfigureAwait(false);
            return _bufferedConfig;
        }

        

        private Task SaveConfigAsync(WebServerConfig config, CancellationToken cancellationToken)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_configFilePath));
                var serialized = JsonConvert.SerializeObject(config, Formatting.Indented);
                return File.WriteAllTextAsync(_configFilePath, serialized, cancellationToken);
            }
            catch (Exception e)
            {
                Logger.Log(e, "Error while save config to file", LogLevel.Error);
                return Task.CompletedTask;
            }
        }

        private async Task<WebServerConfig> TryLoadConfigAsync(CancellationToken cancellationToken)
        {
            try
            {
                var serializedData = await File.ReadAllTextAsync(_configFilePath, cancellationToken).ConfigureAwait(false);
                return JsonConvert.DeserializeObject<WebServerConfig>(serializedData);
            }
            catch (Exception e)
            {
                Logger.Log(e, "Error while load config from file", LogLevel.Error);
                return null;
            }
        }
    }
}
