﻿using System;
using Core.Messages;

namespace Muxers.InMemoryHlsMuxer
{
    internal class ChunkDurationWatcher
    {
        public bool IsNeedNewChunk => _currentDuration >= MaxChunkDuration;

        public TimeSpan MaxChunkDuration { get; }

        private TimeSpan _currentDuration;

        private DateTime? _lastTimeStamp;

        public ChunkDurationWatcher(TimeSpan maxChunkDuration)
        {
            MaxChunkDuration = maxChunkDuration;
        }

        public void AddFrame(VideoMessage videoMessage)
        {
            if (_lastTimeStamp == null)
            {
                _lastTimeStamp = videoMessage.Timestamp;
                return;
            }

            var diff = videoMessage.Timestamp - _lastTimeStamp.Value;
            _currentDuration += diff;

            _lastTimeStamp = videoMessage.Timestamp;
        }

        public void OnNewChunk()
        {
            _lastTimeStamp = null;
            _currentDuration = TimeSpan.Zero;
        }
    }
}
