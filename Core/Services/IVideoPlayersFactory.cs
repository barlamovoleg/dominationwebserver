﻿using System.Threading;
using System.Threading.Tasks;

namespace Core.Services
{
    public interface IVideoPlayersFactory
    {
        Task<IVideoPlayer> CreateVideoPlayerAsync(IRawVideoSource rawVideoSource, IChannel channel, CancellationToken cancellationToken);
    }
}
