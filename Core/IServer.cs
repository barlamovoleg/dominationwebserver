﻿using System;
using System.Collections.ObjectModel;

namespace Core
{
    public interface IServer : IDisposable
    {
        event EventHandler Disconnected; 

        event EventHandler ConnectionLost;

        event EventHandler ConnectionRestored;

        bool IsConnected { get; }

        ReadOnlyObservableCollection<IChannel> Channels { get; }

        void Disconnect();
    }
}
