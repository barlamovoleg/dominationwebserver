#include "stdafx.h"
#include "VideoDecoder.h"

VideoDecoder::VideoDecoder() :
    _context(nullptr)
{
}

VideoDecoder::~VideoDecoder()
{
    avcodec_free_context(&_context);
}

DecoderError VideoDecoder::OpenDecoderContext(AVCodec* codec, AVCodecParameters* parameters)
{
    if (codec == nullptr)
        return DecoderError::FindDecoder;

    _context = avcodec_alloc_context3(codec);
    if (_context == nullptr)
        return DecoderError::AllocContext;

    if (parameters != nullptr)
    {
        if (avcodec_parameters_to_context(_context, parameters) < 0)
            return DecoderError::AllocContext;
    }

    if ((codec->capabilities & AV_CODEC_CAP_TRUNCATED) != 0)
        _context->flags |= AV_CODEC_FLAG_TRUNCATED;

    const int32_t result = avcodec_open2(_context, codec, nullptr);
    if (result < 0)
        return DecoderError::CodecOpen;

    return DecoderError::NoErrors;
}

int32_t VideoDecoder::Push(uint8_t data[], const int32_t size) const
{
    AVPacket packet;
    av_init_packet(&packet);
    packet.data = data;
    packet.size = size;

    return Push(&packet);
}

int32_t VideoDecoder::Push(const AVPacket* packet) const
{
    const int32_t sendErrorCode = avcodec_send_packet(_context, packet);
    return sendErrorCode;
}

int32_t VideoDecoder::Pop(AVFrame* frame) const
{
    const int32_t recvErrorCode = avcodec_receive_frame(_context, frame);
    return recvErrorCode;
}
