﻿import { GridType, VideoGrid } from "../models/gridTypes";
import { Injectable } from '@angular/core';

@Injectable()
export class VideoGridsService {
    public allGrids: VideoGrid[] = [];

    public selectedVideoGrid: VideoGrid;

    private iconExtension: string = ".svg";

    constructor() {

        var count = Object.keys(GridType).length / 2;
        this.allGrids = new Array<VideoGrid>(count);

        this.allGrids[0] = new VideoGrid(GridType.Grid1x1, this.findIconForGridType(GridType.Grid1x1));
        this.allGrids[1] = new VideoGrid(GridType.Grid2x2, this.findIconForGridType(GridType.Grid2x2));
        this.allGrids[2] = new VideoGrid(GridType.Grid5, this.findIconForGridType(GridType.Grid5));
        this.allGrids[3] = new VideoGrid(GridType.Grid6, this.findIconForGridType(GridType.Grid6));
        this.allGrids[4] = new VideoGrid(GridType.Grid3x3, this.findIconForGridType(GridType.Grid3x3));
        this.allGrids[5] = new VideoGrid(GridType.Grid4x4, this.findIconForGridType(GridType.Grid3x3));

        this.selectedVideoGrid = this.allGrids[1];
    }

    public findIconForGridType(gridType: GridType): string {
        var gridTypeName = GridType[gridType];
        return "/dist/images/grids/" + gridTypeName + this.iconExtension;
    }
}