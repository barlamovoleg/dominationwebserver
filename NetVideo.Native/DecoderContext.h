#pragma once

#include "stdafx.h"
#include "Decoder.h"

class DecoderContext
{
private:
	AVCodecContext* _context;
	AVFrame* _decodedFrame;

public:
	explicit DecoderContext();
	virtual ~DecoderContext();
    DecoderError OpenDecoderContext(AVCodec* codec, AVCodecParameters* parameters);

    int32_t Push(uint8_t data[], int32_t size) const;
	int32_t Push(const AVPacket* packet) const;
	int32_t Pop() const;

    int32_t Decode(const AVPacket* packet, DecodeCB callback, void* state) const;
    int32_t Decode(uint8_t data[], int32_t size, DecodeCB callback, void* state) const;
	int32_t Decode(uint8_t data[], int32_t size) const;
	int32_t Decode(const AVPacket* packet) const;

	AVFrame* GetCurrentFrame() const;
};
