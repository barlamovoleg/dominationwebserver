﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Common.Annotations;
using Core;

namespace ServerLogic.VideoPlaying
{
    internal class SimpleVideoPlayer : IVideoPlayer
    {
        public string Name { get; set; }
        public string ContentType { get; } = "application/x-mpegURL";

        public Stream VideoStream => Muxer.OutputStream;

        [NotNull] private IChannel Channel { get; }
        [NotNull] private IVideoMuxer Muxer { get; }
        [NotNull] private IRawVideoSource RawVideoSource { get; }

        public SimpleVideoPlayer([NotNull] IVideoMuxer muxer, [NotNull] IRawVideoSource rawVideoSource, IChannel channel)
        {
            Muxer = muxer ?? throw new ArgumentNullException(nameof(muxer));
            RawVideoSource = rawVideoSource ?? throw new ArgumentNullException(nameof(rawVideoSource));
            Channel = channel;
        }

        public Task InitializeAsync(CancellationToken cancellationToken)
        {
            Muxer.StartMuxing(RawVideoSource.VideoStream);
            return Muxer.WaitFirstChunkAsync(cancellationToken);
        }

        public bool ChannelMatch(IChannel channel)
        {
            return channel != null && channel.ServerOwner == Channel.ServerOwner && channel.ChannelID == Channel.ChannelID;
        }

        public void Dispose()
        {
            Muxer.StopMuxing();

            Muxer.Dispose();
            RawVideoSource.Dispose();
        }
    }
}
