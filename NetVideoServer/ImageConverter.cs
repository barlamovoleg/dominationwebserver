﻿using Core.Messages;
using NetVideo;

namespace NetVideoServer
{
    internal static class ImageConverter
    {
        public static VideoMessage ToVideoMessage(this NetVideoImage netVideoImage)
        {
            return new VideoMessage
            {
                Data = FrameDataFactory.FromBytes(netVideoImage.Data),
                Timestamp = netVideoImage.Timestamp,
                ChannelID = netVideoImage.ChannelID,
                KeyFrameIdx = netVideoImage.KeyFrameIdx,
                IsInvertedColors = netVideoImage.IsInvertedColors,
                IsKeyFrame = netVideoImage.IsKeyFrame,
                IsSubstream = netVideoImage.IsSubstream,
                SequenceID = netVideoImage.PlayerSequenceID,
                HasAlarm = netVideoImage.Alarm == AlarmType.Alarm
            };
        }
    }
}
