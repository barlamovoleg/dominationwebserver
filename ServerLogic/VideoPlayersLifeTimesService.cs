﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using Core;
using Core.Services;

namespace ServerLogic
{
    internal class VideoPlayersLifeTimesService : IVideoPlayersLifeTimesService
    {
        public event EventHandler<IVideoPlayer> PlayerLifeTimeExpired;

        private const int LifeTimeTickPeriodMs = 5 * 1000;
        private const int ViewerLifeTimeDurationMs = 1000 * 60;

        private readonly Timer _lifeTimeTicksTimer;
        private readonly ConcurrentDictionary<IVideoPlayer, DateTime> _lifeTimes = new ConcurrentDictionary<IVideoPlayer, DateTime>();
        private static readonly TimeSpan ViewerLifeTimeSpan = TimeSpan.FromMilliseconds(ViewerLifeTimeDurationMs);

        public VideoPlayersLifeTimesService()
        {
            var periodTimeSpan = TimeSpan.FromMilliseconds(LifeTimeTickPeriodMs);
            _lifeTimeTicksTimer = new Timer(LifeTimeTick, null,periodTimeSpan ,periodTimeSpan );
        }

        public void Dispose()
        {
            _lifeTimeTicksTimer.Dispose();
            _lifeTimes.Clear();
        }

        public void CreateLifeTime(IVideoPlayer videoPlayer)
        {
            _lifeTimes.AddOrUpdate(videoPlayer, DateTime.Now, (player, time) => DateTime.Now);
        }

        public void ResetLifeTime(IVideoPlayer videoPlayer)
        {
            _lifeTimes[videoPlayer] = DateTime.Now;
        }

        private void LifeTimeTick(object state)
        {
            var viewersExpired = new ConcurrentQueue<IVideoPlayer>();

            foreach (var lifeTime in _lifeTimes)
            {
                var player = lifeTime.Key;
                var lastResetTime = lifeTime.Value;
                var elapsed = DateTime.Now - lastResetTime;
                if (elapsed > ViewerLifeTimeSpan)
                    viewersExpired.Enqueue(player);
            }

            while (viewersExpired.TryDequeue(out var viewer))
            {
                PlayerLifeTimeExpired?.Invoke(this, viewer);
            }
        }
    }
}
