﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Common;
using Common.Annotations;
using Core;
using Core.Logging;
using Core.Services;

namespace ServerLogic
{
    internal class ChannelsProvider : IChannelsProvider, IDisposable
    {
        public IServersProvider ServersProvider { get; }

        private readonly object _channelsLocker = new object();
        private readonly List<IChannel> _channels = new List<IChannel>();

        private readonly object _channelsCollectionsHandlersLocker = new object();
        private readonly Dictionary<IServer, CollectionChangedHandler<IChannel>> _channelsCollectionsHandlers
            = new Dictionary<IServer, CollectionChangedHandler<IChannel>>();

        public ChannelsProvider([NotNull] IServersProvider serversProvider)
        {
            ServersProvider = serversProvider ?? throw new ArgumentNullException(nameof(serversProvider));

            OnServersConnected(ServersProvider.GetConnectedServers());

            ServersProvider.ServerConnected += ServersProviderOnServerConnected;
            ServersProvider.ServerDisconnected += ServersProviderOnServerDisconnected;
        }

        public IReadOnlyList<IChannel> GetChannels()
        {
            lock (_channelsLocker)
            {
                return _channels.ToArray();
            }
        }

        public IChannel FindChannelByID(int channelID)
        {
            lock (_channelsLocker)
            {
                return _channels.FirstOrDefault(channel => channel.ChannelID == channelID);
            }
        }

        public IChannel FindChannelByMarker(string channelMarker)
        {
            try
            {
                lock (_channelsLocker)
                {
                    return _channels.FirstOrDefault(channel => IsMarkersEquals(channelMarker, channel.GetMarker()));
                }
            }
            catch (Exception e)
            {
                Logger.Log(e, $"Invalid channel marker {channelMarker}");
                return null;
            }
        }

        private bool IsMarkersEquals(string marker1, string marker2)
        {
            return string.Compare(marker1, marker2, CultureInfo.InvariantCulture, CompareOptions.IgnoreCase) == 0;
        }

        private void ServersProviderOnServerDisconnected(object sender, IServer server)
        {
            OnServersDisconnected(new List<IServer> { server});
        }

        private void ServersProviderOnServerConnected(object sender, IServer server)
        {
            OnServersConnected(new List<IServer> { server });
        }

        private void OnServersDisconnected(IEnumerable<IServer> disconnectedServers)
        {
            lock (_channelsCollectionsHandlersLocker)
            {
                foreach (var disconnectedServer in disconnectedServers)
                {
                    if (_channelsCollectionsHandlers.ContainsKey(disconnectedServer))
                    {
                        _channelsCollectionsHandlers.Remove(disconnectedServer);
                        var handler = _channelsCollectionsHandlers[disconnectedServer];
                        handler.Dispose();
                    }

                    lock (_channelsLocker)
                    {
                        var disconnectedChannels = _channels.Where(channel => channel.ServerOwner == disconnectedServer).ToList();
                        foreach (var disconnectedChannel in disconnectedChannels)
                        {
                            _channels.Remove(disconnectedChannel);
                        }
                    }
                }
            }
        }

        private void OnServersConnected(IEnumerable<IServer> connectedServers)
        {
            lock (_channelsCollectionsHandlersLocker)
            {
                foreach (var connectedServer in connectedServers)
                {
                    AddChannels(connectedServer.Channels);
                    var handler = new CollectionChangedHandler<IChannel>(connectedServer.Channels, OnChannelsConnected);

                    _channelsCollectionsHandlers.Add(connectedServer, handler);
                }
            }
        }

        private void OnChannelsConnected(IEnumerable<IChannel> connectedChannels)
        {
            AddChannels(connectedChannels);
        }

        private void AddChannels(IEnumerable<IChannel> channels)
        {
            lock (_channelsLocker)
            {
                foreach (var channel in channels)
                {
                    _channels.Add(channel);
                }
            }
        }

        public void Dispose()
        {
            ServersProvider.ServerConnected -= ServersProviderOnServerConnected;
            ServersProvider.ServerDisconnected -= ServersProviderOnServerDisconnected;

            lock (_channelsCollectionsHandlersLocker)
            {
                foreach (var channelsCollectionsHandler in _channelsCollectionsHandlers)
                {
                    channelsCollectionsHandler.Key.Dispose();
                }

                _channelsCollectionsHandlers.Clear();
            }

            lock (_channelsLocker)
            {
                _channels.Clear();
            }
        }
    }
}
